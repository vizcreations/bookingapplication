# Makefile for booking console Java

BINNAME = BookgApp.jar
DATADIR = _data
IMGDIR = _img
MFFILE = Manifest.txt
README = README.md
PKG = com/vizcreations/bookg
SRC_CIL = $(PKG)/*.java
CL_CIL = $(PKG)/*.class

all: compile archive

compile:
	$(RM) $(BINNAME)
	$(RM) $(CL_CIL)
	javac $(SRC_CIL)

archive:
	jar -cvfm0 $(BINNAME) $(MFFILE) $(CL_CIL) $(DATADIR) $(IMGDIR) $(README)

clean:
	$(RM) $(BINNAME)
	$(RM) $(CL_CIL)

# caution
wipe:
	$(RM) $(DATADIR)/*.txt
