/* main class to start application - Booking application for Linux */
package com.vizcreations.bookg;

import java.io.FileReader; // unicode lines
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Scanner; // scans a line and breaks it to tokens
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.IOException;
import java.lang.NumberFormatException;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

//import org.apache.log4j.Logger;
//import org.apache.log4j.BasicConfigurator;

public class BookgApp extends JFrame {

//	private static Logger logger;
//	private static List<RoomType> roomTypes; // globally available
//	private static List<Room> rooms;
//	private static List<Booking> bookgs;
//	private static List<Customer> custs;
//	private static List<BookgRoom> bookgRooms;
	private static Iterator iter;
	private static Date myDate;
	private static Date checkInDate;
	private static Date checkOutDate;
	private static long rawTime;
	private static long checkInTime; // in seconds
	private static long checkOutTime;
	private static long noOfNights;
	private static double roomTotal;
	private static int extraBeds;
	private static double extraBedPrice;
	private static double sTax;
	private static double VAT;
	private static double total;
//	private static Pattern pattern;
//	private static Matcher matcher;
//	private static String dateRegex = "(\\d+)(?:\\-)(\\d+)(?:\\-)(\\d+)";

	public static long bookgCount = 0;
	private static long roomTCount = 0;
	private static long roomCount = 0;
	private static long custCount = 0;
	private long bookgRCount = 0;
	private String selectedId;
	private String[] mons;
	private Calendar cal;
	private Date curDate;
	private Date nextDate;
	private int curDay, curMon, curYear;
	private int nextDay, nextMon, nextYear;

	// single frame
	JFrame mainFrame;
	JFrame configFrame;
	JFrame roomTFrame;
	JFrame roomFrame;

	public static DefaultTableModel mainModel;

	/* prepare splash */
	static void renderSplashFrame(Graphics2D g, int frame) {
		final String[] comps = {"Room types", "Rooms", "Configuration", "Bookings"};
		g.setComposite(AlphaComposite.Clear);
		g.fillRect(120, 140, 200, 40);
		g.setPaintMode();
		g.setColor(Color.BLACK);
//		g.drawString("Loading " + comps[(frame / 5) % 3] + "...", 120, 150);
		g.drawString("Loading " + comps[frame] + "...", 120, 150);
	}

	public BookgApp() { // constructor
		super("Hotel Booking Base - " + Config.getHotelName());

//		Customer cust;
		int row = 0;
//		int count = bookgs.size();
//		JTable table;
		final DefaultTableModel tableModel = new DefaultTableModel();
		JLabel heading;
		JLabel footer;
		int i, j;

		/* Search filter controls */
//		GridLayout searchGrid;
		JLabel fDateL, tDateL;
		JComboBox fDateCmb, fMonCmb, fYrCmb;
		JComboBox tDateCmb, tMonCmb, tYrCmb;
		JButton searchBtn;

		mainFrame = this; /* make this available to all nested classes and private methods */
		configFrame = new JFrame("Configuration");
		roomTFrame = new JFrame("Room types");
		roomFrame = new JFrame("Rooms");

		mons = new String[] { "January",
					"February",
					"March",
					"April",
					"May",
					"June",
					"July",
					"August",
					"September",
					"October",
					"November",
					"December"
		};

		cal = Calendar.getInstance();
		curDate = new Date();
		curDay = curDate.getDate();
		curMon = curDate.getMonth();
		curYear = curDate.getYear()+1900;
		cal.setTime(curDate);
		cal.add(Calendar.DATE, 1); // add 1 day
		nextDate = cal.getTime();
		nextDay = nextDate.getDate();
		nextMon = nextDate.getMonth();
		nextYear = nextDate.getYear()+1900;

		/* initialize saved configuration */
		Config.init();
		/* setup gui */
		this.setSize(800, 375);
//		this.getContentPane().setLayout(null);
		this.setTitle("Hotel Booking Base - " + Config.getHotelName());

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationByPlatform(true);
		JPanel mainPane = new JPanel(); //this contains the table
		JPanel topPane = new JPanel();
		JPanel headingPane = new JPanel(); // controls
		JPanel searchPane = new JPanel();
		JPanel midPane = new JPanel();
		JPanel statusPane = new JPanel();

		mainPane.setOpaque(true); // content panes are always opaque
		topPane.setOpaque(true);
		headingPane.setOpaque(true);
		headingPane.setBackground(Color.WHITE);
		searchPane.setOpaque(true);
		midPane.setOpaque(true);
		statusPane.setOpaque(true);
//		searchPane.setLocation(new Point(50, 50));

		String[] columnNames = {"Id", "Customer", "Check-In", "Check-out", "Pax" ,"Total"};
		Object[][] data = {
				        {new Integer(1), "Smith",
				         "7-3-2018", "10-3-2018", new Integer(2), new Double(2500.00)},
				        {new Integer(2), "Doe",
				         "18-3-2018", "21-3-2018", new Integer(2), new Double(4500.00)},
				        {new Integer(3), "Black",
				         "26-4-2018", "28-4-2018", new Integer(2), new Double(2500.00)},
				        {new Integer(4), "White",
				         "13-4-2018", "16-4-2018", new Integer(2), new Double(2500.00)},
				        {new Integer(5), "Brown",
				         "19-4-2018", "23-4-2018", new Integer(2), new Double(4500.00)}
				        };

		bookgCount = 5;
/*
		Object[][] data = new Object[count][6];
		for(Booking bookg:bookgs) {
			cust = bookg.getCust();
			checkInTime = bookg.getCheckIn()*1000;
			Date cI = new Date(checkInTime);
			int cIY = cI.getYear();
			int cIM = cI.getMonth()+1;
			int cID = cI.getDate();
			checkOutTime = bookg.getCheckOut()*1000;
			Date cO = new Date(checkOutTime);
			int cOY = cO.getYear();
			int cOM = cO.getMonth()+1; // java month is in array index
			int cOD = cO.getDate();

			data[row][0] = String.valueOf(bookg.getId());
			data[row][1] = cust.getName();
			data[row][2] = cID+"-"+cIM+"-"+cIY;
			data[row][3] = cOD+"-"+cOM+"-"+cOY;
			data[row][4] = String.valueOf(bookg.getPax());
			data[row][5] = String.valueOf(bookg.getTotalCost());
			++row;
				
		}
*/

		/* table model for data */
		mainModel = new DefaultTableModel(data, columnNames) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		};
//		final JTable table = new JTable(data, columnNames);
		final JTable table = new JTable();
		table.setModel(mainModel);
//		tableModel = BookgList.getTableModel();
//		tableModel = RoomList.getTableModel();
//		tableModel.setColumnIdentifiers(columnNames);
//		table = new JTable(tableModel);
		table.setPreferredScrollableViewportSize(new Dimension(780, 130));
		table.setFillsViewportHeight(true);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(780, 150));

		midPane.setPreferredSize(new Dimension(800, 150));
		midPane.add(scrollPane);
		midPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
//		scrollPane.setBounds(0, 20, 780, 130);
//		scrollPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));

		heading = new JLabel();
		heading.setText("Filter by date");
		heading.setBounds(10, 10, 200, 40);
		heading.setFont(new Font("Sans", Font.PLAIN, 20));

		footer = new JLabel();
		footer.setText("(c) vizcreations 2018");
		footer.setBounds(0, 0, 800, 20);
		statusPane.setLayout(new BorderLayout());
		statusPane.add(footer);
		statusPane.setPreferredSize(new Dimension(800, 30));
		statusPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		topPane.setPreferredSize(new Dimension(780, 110));

		// setup search controls
		searchPane.setPreferredSize(new Dimension(760, 70));

		searchPane.setLayout(null);
		fDateL = new JLabel();
		fDateL.setText("From date");
		fDateL.setBounds(10, 0, 120, 25);
		searchPane.add(fDateL);
		fDateCmb = new JComboBox();
		fDateCmb.addItem("Day");

		// set up days in combo
		for(i=1; i<32; i++) {
			fDateCmb.addItem(String.valueOf(i));
			if(curDay == i) {
				fDateCmb.setSelectedIndex(i);	
			}
		}

		fDateCmb.setBounds(140, 0, 70, 25);
		searchPane.add(fDateCmb);

		fMonCmb = new JComboBox();
		fMonCmb.addItem("Month");

		i = 0;
		for(String mon: mons) {
			fMonCmb.addItem(mon);
			if(curMon == i) {
				fMonCmb.setSelectedIndex(i+1); // because first index in combo is default string
			}

			++i;
		}

		fMonCmb.setBounds(220, 0, 120, 25);
		searchPane.add(fMonCmb);

		fYrCmb = new JComboBox();
		fYrCmb.addItem("Year");

		for(j=curYear,i=0; j<curYear+4; j++,i++) {
			fYrCmb.addItem(String.valueOf(j));
			if(curYear == j) {
				fYrCmb.setSelectedIndex(i+1); // because first index is a default string
			}
		}

		fYrCmb.setBounds(350, 0, 100, 25);
		searchPane.add(fYrCmb);

		tDateL = new JLabel();
		tDateL.setText("To date");
		tDateL.setBounds(10, 35, 120, 25);
		searchPane.add(tDateL);

		tDateCmb = new JComboBox();
		tDateCmb.addItem("Day");
		for(i=1; i<32; i++) {
			tDateCmb.addItem(String.valueOf(i));
			if(nextDay == i) {
				tDateCmb.setSelectedIndex(i);
			}
		}

		tDateCmb.setBounds(140, 35, 70, 25);
		searchPane.add(tDateCmb);

		tMonCmb = new JComboBox();
		tMonCmb.addItem("Month");

		i = 0;
		for(String mon: mons) {
			tMonCmb.addItem(mon);
			if(curMon == i) {
				tMonCmb.setSelectedIndex(i+1);
			}

			++i;
		}
		tMonCmb.setBounds(220, 35, 120, 25);
		searchPane.add(tMonCmb);

		tYrCmb = new JComboBox();
		tYrCmb.addItem("Year");

		for(j=nextYear,i=0; j<nextYear+4; j++,i++) {
			tYrCmb.addItem(String.valueOf(j));
			if(nextYear == j) {
				tYrCmb.setSelectedIndex(i+1);
			}
		}
		tYrCmb.setBounds(350, 35, 100, 25);
		searchPane.add(tYrCmb);

		searchBtn = new JButton();
		searchBtn.setText("Search");
		searchBtn.setBounds(460, 35, 90, 25);
		searchPane.add(searchBtn);

//		searchPane.setBounds(0, 50, 800, 100);
//		searchPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0)); // top, left, bottom, right spacing
		topPane.add(searchPane);
		topPane.setBorder(BorderFactory.createTitledBorder("Available rooms filter by date"));

		mainPane.setLayout(new FlowLayout());
		mainPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
		mainPane.add(topPane);
		// adds a scroll to table
//		mainPane.add(scrollPane);
		mainPane.add(midPane);
		mainPane.add(statusPane);
		this.setContentPane(mainPane);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setOpaque(true);
		menuBar.setPreferredSize(new Dimension(480, 20));

		JMenu fileMenu = new JMenu("File");
		JMenu bookgMenu = new JMenu("Booking");
		JMenu settingsMenu = new JMenu("Settings");
		JMenu helpMenu = new JMenu("Help");

		JMenuItem fileExpBMItem = new JMenuItem("Export records");
		JMenuItem bookgNMItem = new JMenuItem("New");
		JMenuItem bookgCalMItem = new JMenuItem("Calendar");
		JMenuItem bookgExpMItem = new JMenuItem("Export to file");
//		bookgNMItem.setMinimumSize(new Dimension(120, 25));
		JMenuItem exitMItem = new JMenuItem("Exit");
		JMenuItem roomTMItem = new JMenuItem("Room types");
		JMenuItem roomMItem = new JMenuItem("Rooms");
		JMenuItem custMItem = new JMenuItem("Customers");
		JMenuItem configMItem = new JMenuItem("Configuration");
		JMenuItem invoiceMItem = new JMenuItem("Print Invoice");
		JMenuItem aboutMItem = new JMenuItem("About");

		bookgNMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { // is called asynchronously => thread safe?
				showBookgAddGUI(mainModel);
			}
		});

		exitMItem.addActionListener(new ActionListener() { // inner class
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		roomTMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showRoomTLGUI();
			}
		});

		roomMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showRoomLGUI();
			}
		});

		custMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showCustGUI();
			}
		});

		configMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showConfigGUI();
			}
		});

		invoiceMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showInvoiceGUI();
			}
		});

		/* prompt a true dialog window instead of optionpane */
		aboutMItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
/*
				JOptionPane.showMessageDialog(mainFrame,
						"Hotel Booking Application Base 1.0 is a very handy booking\r\n" +
						"solution for any motel/lodge/hotel, especially when there are\r\n" +
						"a lot of rooms to be managed.\r\n\r\n" +
//						"This is a Java application.\r\n\r\n" +
						"vizcreations",
						"About Hotel Booking Application Base 1.0",
						JOptionPane.INFORMATION_MESSAGE);
*/
				AboutDlg aboutDlg = new AboutDlg(mainFrame); // global var
				aboutDlg.show();
			}
		});

		fileMenu.add(fileExpBMItem);
		fileMenu.add(exitMItem);
		bookgMenu.add(bookgNMItem);
		bookgMenu.add(bookgCalMItem);
		bookgMenu.add(bookgExpMItem);
		settingsMenu.add(roomTMItem);
		settingsMenu.add(roomMItem);
		settingsMenu.add(custMItem);
		settingsMenu.add(configMItem);
		settingsMenu.add(invoiceMItem);
		helpMenu.add(aboutMItem);
//		JMenu menuThree = new JMenu("Help");
		menuBar.add(fileMenu);
		menuBar.add(bookgMenu);
		menuBar.add(settingsMenu);
		menuBar.add(helpMenu);
//		menuBar.setFont(new Font("Sans", Font.PLAIN, 18));
		this.setJMenuBar(menuBar);

		/* render the splash image */
		final SplashScreen splash = SplashScreen.getSplashScreen();
		if(splash == null) {
			System.out.println("SplashScreen.getSplashScreen() returned null");
			return;
		}

		Graphics2D g = splash.createGraphics();
		if(g == null) {
			System.out.println("g is null");
			return;
		}

		for(i=0; i<4; i++) {
			renderSplashFrame(g, i);
			splash.update();
			try {
				Thread.sleep(90);
			} catch(InterruptedException e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}
		}

		splash.close();
//		showRoomTGUI();

		/* disallow any resizing of window */
		this.setResizable(false);
//		this.pack();
//		this.setLocationRelativeTo(null);
		centerFrame(this);

		/* testing purpose, needs to be commented out always */
//		showBookgAddGUI(tableModel);

	}

	private void centerFrame(JFrame frame) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);

		frame.setLocation(x, y);
	}

//	public void actionPerformed(ActionEvent e) {
	private void showRoomTLGUI() {

		// room types window
		RoomTypeGUI roomTFrame = new RoomTypeGUI(false, roomTCount);
	}

	private void showRoomTAddGUI(DefaultTableModel tableModel) {
		// create gui
		++roomTCount;
	}

	private void showRoomTEditGUI(long id, DefaultTableModel tableModel) {
//		roomTFrame.setVisible(true);

	}

	private void showRoomLGUI() {
		// show rooms list window
//			JFrame roomFrame = new JFrame("Rooms");
//				roomTFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		RoomGUI roomFrame = new RoomGUI(false, roomCount);
	}

	public void showRoomAddGUI(DefaultTableModel tableModel) {
//		++roomCount;
//		JFrame roomFrame = new RoomGUI(false, roomCount, tableModel);
//		roomFrame.setLocationByPlatform(true);

//		roomFrame.setVisible(true);
	}

	public void showRoomEditGUI(long id, DefaultTableModel tableModel) {
//		JFrame roomFrame = new RoomGUI(true, id, tableModel);
//		roomFrame.setLocationByPlatform(true);

//		roomFrame.setVisible(true);
	}

	private void showBookgAddGUI(DefaultTableModel tableModel) {
//		++bookgCount;
		JFrame bookgFrame = new BookingGUI(bookgCount, tableModel);
		bookgFrame.setLocationByPlatform(true);
		
		bookgFrame.setVisible(true);
	}

	private void showCustGUI() {
		// show customers list window
		JFrame custFrame = new JFrame("Customers");
//		roomTFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		custFrame.setSize(640, 300);
		custFrame.setLocationByPlatform(true);
		custFrame.setResizable(false);

		JPanel custContentPane = new JPanel();
//		custContentPane.setLayout(new GridLayout(2, 1));
		custContentPane.setLayout(new FlowLayout()); // fixed
		custContentPane.setOpaque(true);
		custContentPane.setPreferredSize(new Dimension(620, 300));
//		int row = 0;
//		int count = bookgs.size();
		String[] columnNames = { "Booking Id", "Name", "Email", "Phone", "Address", "Age" };
		Object[][] data = {
				{ "1", "Smith", "smith@vizcreations.com", "9191919199", "Chennai", "22" },
				{ "2", "Doe", "doe@vizcreations.com", "1919191919", "Hyderabad", "33" },
				{ "3", "Black", "black@vizcreations.com", "2022020020", "Chennai", "45" },
				{ "4", "White", "white@vizcreations.com", "1101002222", "Vizag", "25" },
				{ "5", "Brown", "brown@vizcreations.com", "000000000", "Chennai", "33" }
		};

//		Object[][] data = new Object[count][6];
//		for(Booking bookg: bookgs) { // each is a row
//			Customer cust = bookg.getCust();
//			data[row][0] = String.valueOf(cust.getBookgId());
//			data[row][1] = cust.getName();
//			data[row][2] = cust.getEmail();
//			data[row][3] = cust.getPhone();
//			data[row][4] = cust.getAddr();
//			data[row][5] = String.valueOf(cust.getAge());
//			++row;
//		}

		DefaultTableModel custModel = new DefaultTableModel(data, columnNames) {

			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		};

//		final JTable custTable = new JTable(data, columnNames);
		final JTable custTable = new JTable();
		custTable.setModel(custModel);
//		custTable.setModel(mainModel);
		custTable.setPreferredScrollableViewportSize(new Dimension(620, 250));
		custTable.setFillsViewportHeight(true);

		JScrollPane custScrollPane = new JScrollPane(custTable);
		custScrollPane.setPreferredSize(new Dimension(620, 250));
		JPanel mainPane = new JPanel();
		mainPane.setLayout(new FlowLayout());
		mainPane.setOpaque(true);

		mainPane.add(custScrollPane);
		mainPane.setPreferredSize(new Dimension(620, 400));

//		JPanel panelTwo = new JPanel();
		Box newBox = Box.createHorizontalBox();
		JButton newBtn = new JButton("New Booking");
		newBtn.setBounds(10, 10, 90, 25);
		newBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {

//				DefaultTableModel tableModel = new DefaultTableModel();
				showBookgAddGUI(mainModel);
			}
		});
		newBox.add(newBtn);
		mainPane.add(newBox);

		custContentPane.add(mainPane);
		custFrame.setContentPane(custContentPane);

//		custFrame.pack();
		custFrame.setVisible(true);
	}

	private void showInvoiceGUI() {
		JFrame invoiceFrame = new BookgInvoiceGUI();
//		invoiceFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		invoiceFrame.setLocationByPlatform(true);
		invoiceFrame.setResizable(false);
		invoiceFrame.setVisible(true);
	}

	private void showConfigGUI() {
		configFrame.setSize(420, 460);
//		configFrame.getContentPane().setLayout(null);
		configFrame.setLocationByPlatform(true);

		JPanel newPane = new JPanel();
		JPanel formPane = new JPanel();

		newPane.setLayout(new FlowLayout());
		newPane.setOpaque(true);
		newPane.setPreferredSize(new Dimension(420, 460));

		formPane.setOpaque(true);
		formPane.setLayout(null); // fixed
		formPane.setPreferredSize(new Dimension(400, 410));
		configFrame.setContentPane(newPane);
		configFrame.setResizable(false);

		JLabel hotelNLbl = new JLabel("Hotel Name");
		hotelNLbl.setBounds(20, 20, 150, 25);
		hotelNLbl.setText("Hotel Name");
		final JTextField hotelNTxt = new JTextField();
		hotelNTxt.setBounds(180, 20, 200, 25);
		hotelNTxt.setText(String.valueOf(Config.getHotelName()));

		JLabel hotelRIdLbl = new JLabel("Hotel RegId");
		hotelRIdLbl.setBounds(20, 55, 150, 25);
		final JTextField hotelRIdTxt = new JTextField();
		hotelRIdTxt.setBounds(180, 55, 200, 25);
		hotelRIdTxt.setText(String.valueOf(Config.getHotelRegId()));

		JLabel hotelEstdLbl = new JLabel("Hotel Estd");
		hotelEstdLbl.setBounds(20, 90, 150, 25);
		final JTextField hotelEstdTxt = new JTextField();
		hotelEstdTxt.setBounds(180, 90, 200, 25);
		hotelEstdTxt.setText(String.valueOf(Config.getHotelEstd()));

		JLabel hotelOwnLbl = new JLabel("Hotel Owner");
		hotelOwnLbl.setBounds(20, 125, 150, 25);
		final JTextField hotelOwnTxt = new JTextField();
		hotelOwnTxt.setBounds(180, 125, 200, 25);
		hotelOwnTxt.setText(String.valueOf(Config.getHotelOwner()));

		JLabel hotelAddrLbl = new JLabel("Hotel Address");
		hotelAddrLbl.setBounds(20, 160, 150, 25);
		final JTextArea hotelAddrTxt = new JTextArea();
		hotelAddrTxt.setBounds(180, 160, 200, 50);
		hotelAddrTxt.setLineWrap(true);

		hotelAddrTxt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_TAB) {
					if(e.getModifiers() > 0) {
						hotelAddrTxt.transferFocusBackward();
					} else {
						hotelAddrTxt.transferFocus();
					}

					e.consume();
				}
			}

		});

		hotelAddrTxt.setWrapStyleWord(true);
		hotelAddrTxt.setFocusTraversalKeysEnabled(false);
		hotelAddrTxt.setText(String.valueOf(Config.getHotelAddr()));

		JLabel hotelPhonLbl = new JLabel("Hotel Phone");
		hotelPhonLbl.setBounds(20, 220, 150, 25);
		final JTextField hotelPhonTxt = new JTextField();
		hotelPhonTxt.setBounds(180, 220, 200, 25);
		hotelPhonTxt.setText(String.valueOf(Config.getHotelPhone()));

		JLabel hotelExPrcLbl = new JLabel("Extrabed-Price");
		hotelExPrcLbl.setBounds(20, 255, 150, 25);
		final JTextField hotelExPrcTxt = new JTextField();
		hotelExPrcTxt.setBounds(180, 255, 100, 25);
		hotelExPrcTxt.setText(String.valueOf(Config.getExtraBedPrice()));

		JLabel sTaxLbl = new JLabel("Service Tax");
		sTaxLbl.setBounds(20, 295, 150, 25);
		final JTextField sTaxTxt = new JTextField();
		sTaxTxt.setBounds(180, 295, 50, 25);
		sTaxTxt.setText(String.valueOf(Config.getServiceTax()));

		JLabel vatLbl = new JLabel("VAT");
		vatLbl.setBounds(20, 330, 150, 25);
		final JTextField vatTxt = new JTextField();
		vatTxt.setBounds(180, 330, 50, 25);
		vatTxt.setText(String.valueOf(Config.getVAT()));

		formPane.add(hotelNLbl);
		formPane.add(hotelNTxt);
		formPane.add(hotelRIdLbl);
		formPane.add(hotelRIdTxt);
		formPane.add(hotelEstdLbl);
		formPane.add(hotelEstdTxt);
		formPane.add(hotelOwnLbl);
		formPane.add(hotelOwnTxt);
		formPane.add(hotelAddrLbl);
		formPane.add(hotelAddrTxt);
		formPane.add(hotelPhonLbl);
		formPane.add(hotelPhonTxt);
		formPane.add(hotelExPrcLbl);
		formPane.add(hotelExPrcTxt);
		formPane.add(sTaxLbl);
		formPane.add(sTaxTxt);
		formPane.add(vatLbl);
		formPane.add(vatTxt);

		JButton save = new JButton("Save");
		save.setBounds(180, 365, 90, 25);
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String msg;
				
				// get list pointer from list object

				if(!hotelNTxt.getText().isEmpty()
				&& !hotelRIdTxt.getText().isEmpty()
				&& !hotelOwnTxt.getText().isEmpty()
				) {
					Config.setHotelName(hotelNTxt.getText());
					Config.setHotelRegId(hotelRIdTxt.getText());
					Config.setHotelEstd(hotelEstdTxt.getText());
					Config.setHotelOwner(hotelOwnTxt.getText());
					Config.setHotelAddr(hotelAddrTxt.getText());
					Config.setHotelPhone(hotelPhonTxt.getText());
					Config.setExtraBedPrice(Double.parseDouble(hotelExPrcTxt.getText()));
					Config.setServiceTax(Double.parseDouble(sTaxTxt.getText()));
					Config.setVAT(Double.parseDouble(vatTxt.getText()));

					// now save to flat file
					Config.save(); // saves it's data to file
					msg = "Configuration saved successfully..";
				} else {
					msg = "Please enter fields correctly!";
				}

				JOptionPane.showMessageDialog(configFrame, msg);
			}
		});

		formPane.add(save);
		formPane.setBorder(BorderFactory.createTitledBorder("Setup configuration"));

		newPane.add(formPane);
		configFrame.setVisible(true);
	}

	/* prepare and show gui */
	private static void createAndShowMainWnd() {
		BookgApp mainWnd = new BookgApp();
		mainWnd.setVisible(true);
		mainWnd.toFront();
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowMainWnd();
			}
		});
	}

}
