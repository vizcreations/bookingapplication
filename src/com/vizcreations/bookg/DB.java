/* MySQL functions wrapper */
package com.vizcreations.bookg;

import java.util.Properties;
import java.sql.*;
import java.sql.SQLException;

class DB {

	private static String serverName;
	private static String userName;
	private static String password;
	private static String name;
	private static String dbms;
	private static int portNumber;

	public static String query;
	private static Statement stmt;
	private static Connection con; // class variable never changes
	private static ResultSet rs;

	private String[][] data; // always array of string arrays

	public DB() {
		serverName = "";
		userName = "";
		password = "";
		name = "";
		dbms = "";
		portNumber = 0;

		query = "";
		stmt = null;
		con = null;
		data = null;
		rs = null;
	}

	public static void setServerName(String sServerName) {
		serverName = sServerName;
	}

	public static String getServerName() {
		return serverName;
	}

	public static void setUserName(String sUserName) {
		userName = sUserName;
	}

	public static void setPassword(String sPassword) {
		password = sPassword;
	}

	public static void setName(String sName) {
		name = sName;
	}

	/* mysql etc */
	public static void setDbms(String sDbms) {
		dbms = sDbms;
	}

	public static void setPortNumber(int sPortNumber) {
		portNumber = sPortNumber;
	}

	public static Connection connect() throws SQLException { // no prototypes, direct body
		con = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", userName);
		connectionProps.put("password", password);

		try {
			if(dbms.equals("mysql")) {
				con = DriverManager.getConnection(
						"jdbc:" + dbms+  "://" +
						serverName +
						":" + portNumber + "/" +
						name + "?",
						connectionProps);
			} else if(dbms.equals("derby")) {
				con = DriverManager.getConnection(
						"jdbc:" + dbms + ":" +
						name +
						";create=true",
						connectionProps);
			}


			System.out.println("Connection established..");
		} catch(SQLException sqle) {
//			System.out.println("SQL error: " + sqle.getMessage());
			con = null;
			throw new SQLException(sqle.getMessage());
		}

		return con;
	}

	public static void setQuery(String sQuery) {
		query = sQuery;
	}

	public static String getQuery() {
		return query;
	}

	/* execute member query (update table) */
	public static int execute(boolean update) throws SQLException {
		if(query != null) {
			try {
				stmt = con.createStatement();
				stmt.setEscapeProcessing(true);

				if(update == true)
					stmt.executeUpdate(query);
				else
					rs = stmt.executeQuery(query); // database query execute
			} catch(SQLException sqle) {
//				System.err.println("SQL execution error: " + sqle.getMessage());
				rs = null;
				throw new SQLException(sqle.getMessage());
//				return 1;
			} finally {
/*
				if(stmt != null)
					stmt.close();
*/
			}
		}

		return 0;
	}

	static void freeRes() throws SQLException {
		try {
			if(stmt != null)
				stmt.close(); // close all resources/result-sets
		} catch(SQLException e) {
//			System.err.println("SQL error: " + e.getMessage());
			throw new SQLException(e.getMessage());
		}
	}

	public static String[] getRow() {
		String[] row = { "" };
		return row;
	}

	public static String[][] getRows() {
		String[][] rows = {
				{ "" }
				};
		return rows;
	}

	public static ResultSet getRS() {
		return rs;
	}

	public static String getFirst() throws SQLException {
		String first = "";
		try {
			if(rs != null) {
				if(rs.next()) {
					first = rs.getString(1);
				}
			}
		} catch(SQLException e) {
			first = "";
//			System.err.println("SQL error: " + e.getMessage());
			throw new SQLException(e.getMessage());
		}

		return first;
	}

	public static int getCount() { // current number of rows fetched
		return 0;
	}

	/* close the database connection */
	public static void close() throws SQLException {
		try {
			if(stmt != null)
				stmt.close();

			if(con != null) {
				con.close();
				System.out.println("Connection closed..");
			}
		} catch(SQLException sqle) {
//			System.out.println("SQL error: " + sqle.getMessage());
			throw new SQLException(sqle.getMessage());
		}
	}

}
