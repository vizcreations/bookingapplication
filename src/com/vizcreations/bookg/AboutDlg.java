/* about dialog for hotel booking base extends jdialog */
package com.vizcreations.bookg;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AboutDlg extends JDialog {

	JLabel caption;
	JLabel text;
//	JLabel comp;
	JLabel webaddr;
	JLabel logo;

	public AboutDlg(JFrame parent) {
		super(parent, "About Hotel Booking Base", true);
		JPanel newContentPane = new JPanel();
		newContentPane.setOpaque(true);
		newContentPane.setLayout(null);

		Box vbox = Box.createVerticalBox();
		caption = new JLabel("Hotel Booking Base 1.0");

		text = new JLabel("<html>Hotel Booking Base 1.0 is a very handy room booking software for\n"+
					"any small hotel, motel or guest house developed in Java technology.</html>");

//		comp = new JLabel("vizcreations");
		webaddr = new JLabel("http://vizcreations.com");

		JSeparator hsep = new JSeparator(JSeparator.HORIZONTAL);
		logo = new JLabel(new ImageIcon("_img/logo.png"));

//		vbox.add(caption);
//		vbox.add(text);
//		vbox.add(comp);
//		vbox.add(webaddr);
//		this.getContentPane().add(vbox, "Center");
		text.setBounds(10, 10, 400, 50);
//		comp.setBounds(10, 60, 200, 25);
		hsep.setBounds(10, 80, 400, 10);
		logo.setBounds(10, 95, 200, 25);
		webaddr.setBounds(10, 120, 200, 25);
		newContentPane.add(text);
//		newContentPane.add(comp);
		newContentPane.add(webaddr);
		newContentPane.add(hsep);
		newContentPane.add(logo);

		JPanel btnPane = new JPanel();
		JButton okBtn = new JButton("OK");
		okBtn.setBounds(0, 0, 90, 25);
		btnPane.add(okBtn);
		btnPane.setBounds(0, 150, 420, 50);
		newContentPane.add(btnPane);

//		this.getContentPane().add(btnPane, "South");

		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});

		setContentPane(newContentPane);
		setSize(420, 210);

	}

}
