/* Class defining the full room types window with the add form */
package com.vizcreations.bookg;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

public class RoomTypeGUI extends JFrame implements ActionListener { /* both a window and also listens for events */

	private JLabel heading;
	final JLabel idLbl;
	final JTextField idTxt;
	final JLabel namLbl;
	final JTextField namTxt;
	final JLabel descLbl;
	final JTextArea descTxt;
	final JLabel prcLbl;
	final JTextField prcTxt;
	final JLabel staLbl;
	final JComboBox staTxt;
	final JButton save;
//	final boolean editMode = false;

//	JScrollPane scrollPane;
	JPanel formPane;
	// temporary variable to hold data
//	private List<RoomType> roomTypes;
	private int numClicks = 0;
	private boolean editMode = false;
	private DefaultTableModel tableModel; // pointer
	private int count;

	public RoomTypeGUI(boolean edit, long roomTCount) { /* constructor */
		super("Room types: " + Config.getHotelName());
		Box fullBox = Box.createVerticalBox();
		Box headBox = Box.createVerticalBox(); // full box/block for heading
		Box mainBox = Box.createHorizontalBox();
		Box rightBox = Box.createVerticalBox();

		JPanel headPane = new JPanel();
		JPanel leftPane = new JPanel();
		JPanel btnPane = new JPanel(); // right box buttons

		final JFrame wnd = this; // pointer to self

		headBox.setPreferredSize(new Dimension(480, 35));
		headPane.setLayout(new BorderLayout());
		headPane.setOpaque(true);
		headPane.setPreferredSize(new Dimension(480, 35));
		headPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		leftPane.setLayout(null);
		leftPane.setOpaque(true);

		btnPane.setLayout(null);
		btnPane.setOpaque(true);
		formPane = new JPanel();
		formPane.setOpaque(true);
		formPane.setLayout(null); /* fixed layout */

//		this.setSize(950, 300);
		this.setSize(950, 400);
		this.setLocationByPlatform(true);

		this.editMode = edit;

//		roomTypes = RoomTypeList.getList();

		/* hide maximize */
		this.setResizable(false);
		this.setTitle("Room types - " + Config.getHotelName());
		JPanel newContentPane = new JPanel();
//		JPanel newContentPaneTwo = new JPanel();
		newContentPane.setLayout(new FlowLayout());
		newContentPane.setOpaque(true);
//		newContentPane.setPreferredSize(new Dimension(950, 300));
		newContentPane.setPreferredSize(new Dimension(950, 400));

		headPane.setPreferredSize(new Dimension(480, 35));
		leftPane.setPreferredSize(new Dimension(480, 380));
		rightBox.setPreferredSize(new Dimension(420, 380));
//		newContentPaneTwo.setLayout(null);
//		newContentPaneTwo.setOpaque(true);
		int row = 0;
		int col = 0;
//		int count = roomTypes.size();

		heading = new JLabel("Add/Remove Room Types");
		heading.setBounds(40, 10, 200, 35);

//		heading.setFont(new Font("Sans", Font.BOLD, 12));
//		heading.setBackground(Color.GRAY);
//		heading.setForeground(Color.WHITE);
//		final DefaultTableModel tableModel;// = new DefaultTableModel(0, 0);

		String[] columnNames = { "ID", "Name", "Description", "Price", "Status" };

		Object[][] data = {
				{ "1", "Double bed", "Double bed standard", "1500.00", "ACTIVE" },
				{ "2", "Double bed A/C", "Double bed luxurious A/C", "3500.00", "ACTIVE" },
				{ "3", "Camp house", "Desert Camp House", "14500.00", "ACTIVE" }
		};

		count = 3; // at first

/*
		Object[][] data = new Object[count][5];
		for(RoomType roomT: roomTypes) { // each is a row
//			col = 0;
			data[row][0] = String.valueOf(roomT.getId());
			data[row][1] = roomT.getName();
			data[row][2] = roomT.getDesc();
			data[row][3] = String.valueOf(roomT.getPrice());
//			data[row][4] = String.valueOf(roomT.isActive());
			data[row][4] = (roomT.isActive()) ? "ACTIVE" : "INACTIVE";
			++row;
		}
*/
		tableModel = new DefaultTableModel(data, columnNames) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

//		final JTable roomTTable = new JTable(data, columnNames);
		final JTable roomTTable = new JTable();
		roomTTable.setModel(tableModel);

//		tableModel = new RoomTypeModel();
//		tableModel = RoomTypeList.getTableModel();
//		tableModel.setColumnIdentifiers(columnNames);
//		final JTable roomTTable = new JTable(tableModel);
/*
		for(RoomType roomT: roomTypes) { // each is a row
//			col = 0;
			tableModel.addRow(new Object[] { String.valueOf(roomT.getId()),
							roomT.getName(),
							roomT.getDesc(),
							String.valueOf(roomT.getPrice()),
//			data[row][4] = String.valueOf(roomT.isActive());
							(roomT.isActive()) ? "ACTIVE" : "INACTIVE" });
		}
*/
		roomTTable.setPreferredScrollableViewportSize(new Dimension(480, 250));
		roomTTable.setFillsViewportHeight(true);
/*		int column = 0;
		int sRow = roomTTable.getSelectedRow();
		String value = roomTTable.getModel().getValueAt(sRow, column).toString();
System.out.println("id:" +value);*/
		
/*
		JButton roomTABtn = new JButton("Add new");
		roomTABtn.setBounds(10, 20, 120, 30);
		final JButton roomTEBtn = new JButton("Edit");
		roomTEBtn.setBounds(150, 20, 120, 30);
		roomTABtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showRoomTAddGUI(tableModel);
			}
		});
*/
		
		
//		ListSelectionModel cellSelectionModel = roomTTable.getSelectionModel();
//	    cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

//	    cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
//	    	public void valueChanged(ListSelectionEvent e) {
		roomTTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(final MouseEvent e) {
				if(e.getClickCount() == 1) {
					final JTable roomTTable = (JTable)e.getSource();
//					String selectedId = null;
					final int selectedRow = roomTTable.getSelectedRow();
					final int selectedColumn = 0;
	//	            System.out.println("Selected Row: " + selectedRow);
	//	            for (int i = 0; i < selectedRow.length; i++) {
					String selectedId = 
					(String) roomTTable.getValueAt(selectedRow, selectedColumn);
	//	            }
	//	            System.out.println("Selected Id: " + selectedId);
					roomTTable.removeAll();
				}
			}

		    });

/*
		roomTEBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showRoomTEditGUI(Long.parseLong(selectedId),
								tableModel);
			}
		});
*/

//		newContentPaneTwo.add(roomTABtn);
//		newContentPaneTwo.add(roomTEBtn);

		headPane.add(heading, BorderLayout.LINE_START);

		JScrollPane rmtScrollPane = new JScrollPane(roomTTable);
		rmtScrollPane.setPreferredSize(new Dimension(480, 250));

		rmtScrollPane.setBounds(10, 10, 480, 250);
		leftPane.add(rmtScrollPane); // adding the grid

		JSeparator hsep = new JSeparator(JSeparator.HORIZONTAL);
		hsep.setBounds(10, 280, 480, 10);
		leftPane.add(hsep);

		JButton delBtn = new JButton("Delete");
		delBtn.setBounds(10, 300, 90, 25);
		delBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(wnd, "Coming soon..");
			}
		});
		leftPane.add(delBtn);

		JButton editBtn = new JButton("Edit");
		editBtn.setBounds(390, 300, 90, 25);

		/* some dummy action */
		editBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(wnd, "Coming soon..");
			}
		});
		leftPane.add(editBtn);
//		newContentPane.add(rmtScrollPane);

		/* Form elements here */

//		++roomTCount;
		idLbl = new JLabel("ID");
//		idLbl.setBounds(10, 20, 150, 30);
		idLbl.setText("ID");
		idTxt = new JTextField();
//		idTxt.setBounds(170, 20, 50, 30);
		idTxt.setText(String.valueOf(roomTCount));

		namLbl = new JLabel("Room type name");
		namLbl.setBounds(20, 20, 150, 25);
		namTxt = new JTextField();
		namTxt.setBounds(190, 20, 200, 25);

		descLbl = new JLabel("Description");
		descLbl.setBounds(20, 55, 150, 25);
		descTxt = new JTextArea();

		descTxt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_TAB) {
					if(e.getModifiers() > 0) {
						descTxt.transferFocusBackward();
					} else {
						descTxt.transferFocus();
					}

					e.consume();
				}
			}

		});

		Border border = BorderFactory.createLineBorder(Color.GRAY);
		descTxt.setBorder(
			BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		descTxt.setBounds(190, 55, 200, 50);
		descTxt.setLineWrap(true);
		descTxt.setWrapStyleWord(true);
		descTxt.setFocusTraversalKeysEnabled(false);

		prcLbl = new JLabel("Price");
		prcLbl.setBounds(20, 115, 150, 25);
		prcTxt = new JTextField();
		prcTxt.setBounds(190, 115, 200, 25);

		staLbl = new JLabel("Status");
		staLbl.setBounds(20, 150, 150, 25);
//		staTxt = new JTextField();
		staTxt = new JComboBox();
		staTxt.setBounds(190, 150, 200, 25);
//		staTxt.setText(String.valueOf("1"));
		staTxt.addItem("INACTIVE");
		staTxt.addItem("ACTIVE");

 		save = new JButton("Save");
		save.setBounds(190, 10, 90, 25);
		JButton cancel = new JButton("Cancel");
		cancel.setBounds(290, 10, 90, 25);

		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				wnd.dispose();
			}
		});
//		save.addActionListener(new ActionListener() {
		save.addActionListener(this);

		formPane.add(idLbl);
		formPane.add(idTxt);
		formPane.add(namLbl);
		formPane.add(namTxt);
		formPane.add(descLbl);
		formPane.add(descTxt);
		formPane.add(prcLbl);
		formPane.add(prcTxt);
		formPane.add(staLbl);
		formPane.add(staTxt);

//		rightBox.setBounds(10, 190, 450, 250);
		formPane.setPreferredSize(new Dimension(420, 220));
		rightBox.add(formPane);
		btnPane.add(save);
		btnPane.add(cancel);

		btnPane.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
		rightBox.add(btnPane);

		formPane.setBorder(BorderFactory.createTitledBorder("Add a new room type"));

		headBox.add(headPane);
		fullBox.add(headBox);
		mainBox.add(leftPane);
		mainBox.add(rightBox);
		fullBox.add(mainBox);

//		newContentPane.add(newContentPaneTwo);
//		newContentPane.add(roomTTable);
//		newContentPane.add(formPane);
		newContentPane.setLayout(new BorderLayout());
		newContentPane.add(fullBox, BorderLayout.CENTER);
		this.setContentPane(newContentPane);

		this.pack();
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		// todo
//		RoomType roomT; // model
		long id = 0;
		String name;
		String desc;
		double price;
		boolean isActive = false;
		String msg;
/*				numClicks++;
		if(numClicks > 1) {
			msg = "Sorry you cannot submit twice";
			JOptionPane.showMessageDialog(this, msg);
			System.exit(1);
		}*/
//		roomTypes = new LinkedList<RoomType>();

		// get list pointer from list object
//		roomTypes = RoomTypeList.getList(); // model
//		roomT = new RoomType(); // because we're saving

//		if(RoomTypeList.exists(Long.parseLong(idTxt.getText()),namTxt.getText()) == true) {
//			System.err.println("Roomtype "+namTxt.getText()+" already exists ...");
//			msg = "Roomtype " + namTxt.getText() + " already exists..";
//			JOptionPane.showMessageDialog(this, msg);
//			return;
//		}

		if(!idTxt.getText().isEmpty()
		&& !namTxt.getText().isEmpty()
		&& !prcTxt.getText().isEmpty()
		) {

			if(editMode == true) {
//				roomT = RoomTypeList.get(Long.parseLong(idTxt.getText()));
			}

			name = namTxt.getText();
			price = Double.parseDouble(prcTxt.getText());
			desc = descTxt.getText();
			isActive = ((staTxt.getSelectedItem().toString()).equals("ACTIVE")) ? true : false;
//			roomT.setId(Long.parseLong(idTxt.getText()));
//			roomT.setName(namTxt.getText());
//			roomT.setDesc(descTxt.getText());
//			roomT.setPrice(Double.parseDouble(prcTxt.getText()));
//			roomT.setActive(
//			(Integer.parseInt(staTxt.getSelectedItem().toString()) == 1) ? true : false);
//			if(editMode == false) {
//				roomTypes.add(roomT); // add to the original list pointer
//			}
			// now save to flat file
//			RoomTypeList.save(); // saves it's data to file

			// save to table model
//			if(editMode == false) { // new record
//				tableModel.addRow(new Object[] { String.valueOf(roomT.getId()),
//							roomT.getName(),
//							roomT.getDesc(),
//							String.valueOf(roomT.getPrice()),
//							(roomT.isActive()) ? "ACTIVE" : "INACTIVE" }
//				);
//			}
//			tableModel.fireTableDataChanged();

			/* add new row to table */
			++count; // for ID
			tableModel.addRow(new Object[] { String.valueOf(count),
							name,
							desc,
							String.valueOf(price),
							(isActive) ? "ACTIVE" : "INACTIVE" }
			);
			tableModel.fireTableDataChanged();

			msg = "Room type saved successfully..";
//			this.dispose();
		} else {
			msg = "Please enter fields correctly!";
		}

		JOptionPane.showMessageDialog(this, msg);
	}
}
