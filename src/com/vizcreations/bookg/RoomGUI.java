/* class defining the full rooms window with the add form */
package com.vizcreations.bookg;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class RoomGUI extends JFrame implements ActionListener {
	// controls and other elements
	final JLabel idLbl;
	final JTextField idTxt;
	final JLabel rTIdLbl;
	final JComboBox rTIds;
	final JLabel namLbl;
	final JTextField namTxt;
	final JLabel priceLbl;
	final JTextField priceTxt;
	final JLabel floorLbl;
	final JTextField floorTxt;
	final JLabel staLbl;
	final JComboBox staTxt;
//	final boolean editMode = false;
	final JButton save;

//	private List<Room> rooms;
//	private List<RoomType> roomTypes;
	private int numClicks = 0;
	private DefaultTableModel tableModel;
	private int count = 0;

	public RoomGUI(boolean editMode, long roomCount) { /* constructor */
		super("Rooms - " + Config.getHotelName());
		final JFrame wnd = this;
		Box fullBox = Box.createVerticalBox(); // for two columns
		Box headBox = Box.createVerticalBox();

		JPanel headPane = new JPanel();
		headPane.setOpaque(true);
		headPane.setLayout(new BorderLayout());
		headPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JPanel leftPane = new JPanel();
		leftPane.setOpaque(true);
		leftPane.setLayout(null);

		leftPane.setPreferredSize(new Dimension(450, 350));
		leftPane.setBounds(10, 10, 450, 350);

		Box mainBox = Box.createHorizontalBox();
		Box rightBox = Box.createVerticalBox();
		rightBox.setBounds(0, 0, 420, 350);
		rightBox.setPreferredSize(new Dimension(400, 350));

		JPanel btnPane = new JPanel();
		btnPane.setOpaque(true);
		btnPane.setLayout(null); /* fixed layout */
		btnPane.setPreferredSize(new Dimension(420, 50));
		btnPane.setBounds(0, 0, 420, 50);

//		rooms = RoomList.getList();
//		roomTypes = RoomTypeList.getList();
		this.setSize(900, 400);
		this.setLocationByPlatform(true);
		this.setResizable(false);
		this.setTitle("Rooms - " + Config.getHotelName());

		JLabel heading = new JLabel("Add/Remove Rooms");
		heading.setBounds(10, 10, 900, 25);
		headPane.add(heading);
		headBox.add(headPane);

		fullBox.setBounds(0, 0, 900, 400); // including heading
		mainBox.setBounds(0, 0, 900, 350);
		mainBox.setPreferredSize(new Dimension(900, 350));
		JPanel newContentPane = new JPanel(); // main pane
		JPanel newContentPaneTwo = new JPanel(); // the form pane

//		newContentPane.setLayout(new FlowLayout());
		newContentPane.setOpaque(true);
		newContentPane.setPreferredSize(new Dimension(900, 400)); // same as full box which is contained here

		// form pane
		newContentPaneTwo.setLayout(null);
		newContentPaneTwo.setOpaque(true);
		newContentPaneTwo.setPreferredSize(new Dimension(420, 280));
		newContentPaneTwo.setBounds(0, 10, 420, 280);
//		newContentPaneTwo.setPreferredSize(new Dimension(480, 150));

		int row = 0;
//		int count = rooms.size();
//		final DefaultTableModel tableModel;
		String[] columnNames = { "ID", "Roomtype", "Name", "Price", "Floor", "Status" };
		Object[][] data = {
				{ "1", "Single bed A/C", "Room #103", "3500.00", "1", "ACTIVE" },
				{ "2", "Super Suite Deluxe", "Room #203", "5500.00", "2", "ACTIVE" },
				{ "3", "Single bed", "Room #303", "1500.00", "3", "ACTIVE" },
		};
		count = 3; // initially
/*
		Object[][] data = new Object[count][5];
		for(Room room: rooms) { // each is a row
			data[row][0] = String.valueOf(room.getId());
			data[row][1] = room.getRoomTId();
			data[row][2] = room.getName();
			data[row][3] = String.valueOf(room.getFloor());
			data[row][4] = (room.isActive()) ? "ACTIVE" : "INACTIVE";
			++row;
		}
*/

		tableModel = new DefaultTableModel(data, columnNames) {

			@Override
			public boolean isCellEditable(int row, int column) {
				return false; // all cells uneditable
			}
		};

//		final JTable roomTable = new JTable(data, columnNames);
		final JTable roomTable = new JTable();
		roomTable.setModel(tableModel);

//		tableModel = RoomList.getTableModel();
//		final JTable roomTable = new JTable(tableModel);
		roomTable.setPreferredScrollableViewportSize(new Dimension(450, 250));
		roomTable.setFillsViewportHeight(true);

/*
		JButton roomABtn = new JButton("Add new");
		roomABtn.setBounds(10, 20, 120, 30);
		JButton roomEBtn = new JButton("Edit");
		roomEBtn.setBounds(150, 20, 120, 30);
		roomABtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showRoomAddGUI(tableModel);
			}
		});
		
*/

		roomTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(final MouseEvent e) {
				if(e.getClickCount() == 1) {
					final JTable roomTable = (JTable)e.getSource();
//					String selectedId = null;
					final int selectedRow = roomTable.getSelectedRow();
					final int selectedColumn = 0;
	//	            System.out.println("Selected Row: " + selectedRow);
	//	            for (int i = 0; i < selectedRow.length; i++) {
					String selectedId = 
					(String) roomTable.getValueAt(selectedRow, selectedColumn);
	//	            }
	//	            System.out.println("Selected Id: " + selectedId);
					roomTable.removeAll();
				}
			}

	   	});
		
/*

		roomEBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showRoomEditGUI(Long.parseLong(selectedId),
							tableModel);
			}
		});
		
*/

/*
		newContentPaneTwo.add(roomABtn);
		newContentPaneTwo.add(roomEBtn);
*/

		JScrollPane rmScrollPane = new JScrollPane(roomTable);
		rmScrollPane.setPreferredSize(new Dimension(450, 250));
		rmScrollPane.setBounds(10, 10, 450, 250);

		leftPane.add(rmScrollPane);
		JSeparator hsep = new JSeparator(JSeparator.HORIZONTAL); // horizontal line
		hsep.setPreferredSize(new Dimension(450, 5));
		hsep.setBounds(10, 280, 450, 5);
		leftPane.add(hsep);

		JButton delBtn = new JButton("Delete");
		delBtn.setBounds(10, 300, 90, 25);
		delBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(wnd, "Coming soon..");
			}
		});

		JButton editBtn = new JButton("Edit");
		editBtn.setBounds(330, 300, 90, 25);
		editBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(wnd, "Coming soon..");
			}
		});

		leftPane.add(delBtn);
		leftPane.add(editBtn);

		/* set up controls */

//		++roomCount;
		idLbl = new JLabel("ID");
//		idLbl.setBounds(10, 20, 150, 30);
		idLbl.setText("ID");
		idTxt = new JTextField();
//		idTxt.setBounds(170, 20, 50, 30);
//		idTxt.setText(String.valueOf(roomCount));

		rTIdLbl = new JLabel("Room type Id");
		rTIdLbl.setBounds(30, 20, 150, 25);
		rTIds = new JComboBox();
		rTIds.setBounds(190, 20, 200, 25);
//		for(RoomType roomType: roomTypes) {
//			rTIds.addItem(roomType.getName()); // indexes are ID-1
//		}
		rTIds.addItem("Single bed standard");
		rTIds.addItem("Single bed A/C");
		rTIds.addItem("Double bed A/C");
		rTIds.addItem("Deluxe Suite");

		namLbl = new JLabel("Room name");
		namLbl.setBounds(30, 55, 150, 25);
		namTxt = new JTextField();
		namTxt.setBounds(190, 55, 200, 25);

		priceLbl = new JLabel("Room price");
		priceLbl.setBounds(30, 90, 150, 25);
		priceTxt = new JTextField();
		priceTxt.setBounds(190, 90, 200, 25);

		floorLbl = new JLabel("Floor");
		floorLbl.setBounds(30, 125, 150, 25);
		floorTxt = new JTextField();
		floorTxt.setBounds(190, 125, 200, 25);

		staLbl = new JLabel("Status");
		staLbl.setBounds(30, 160, 150, 25);
		staTxt = new JComboBox();
		staTxt.addItem("INACTIVE");
		staTxt.addItem("ACTIVE");
		staTxt.setBounds(190, 160, 200, 25);

		save = new JButton("Save");
		save.setBounds(190, 10, 90, 25);
		save.addActionListener(this);
		JButton cancel = new JButton("Cancel");
		cancel.setBounds(290, 10, 90, 25);

		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				wnd.dispose();
			}
		});

		btnPane.add(save);
		btnPane.add(cancel);

		newContentPaneTwo.add(idLbl);
		newContentPaneTwo.add(idTxt);
		newContentPaneTwo.add(rTIdLbl);
		newContentPaneTwo.add(rTIds);
		newContentPaneTwo.add(namLbl);
		newContentPaneTwo.add(namTxt);
		newContentPaneTwo.add(priceLbl);
		newContentPaneTwo.add(priceTxt);
		newContentPaneTwo.add(floorLbl);
		newContentPaneTwo.add(floorTxt);
		newContentPaneTwo.add(staLbl);
		newContentPaneTwo.add(staTxt);
//		newContentPaneTwo.add(save);

		newContentPaneTwo.setBorder(BorderFactory.createTitledBorder("Add new room"));
//		rightBox.setBounds(10, 10, 400, 180);

		rightBox.add(newContentPaneTwo);
		rightBox.add(btnPane);

		mainBox.add(leftPane);
		mainBox.add(rightBox);

		fullBox.add(headBox);
		fullBox.add(mainBox);

		newContentPane.setLayout(new BorderLayout());
		newContentPane.add(fullBox, BorderLayout.CENTER);

		this.setContentPane(newContentPane);

		this.pack();
		this.setVisible(true);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		// save record
		String name;
		String type;
		String price;
		String floor;
		String status;

		if(namTxt.getText().isEmpty() == false
		&& floorTxt.getText().isEmpty() == false
		&& priceTxt.getText().isEmpty() == false
		) {
			// todo
			++count; // new id
			name = namTxt.getText();
			type = rTIds.getSelectedItem().toString();
			floor = floorTxt.getText();
			price = priceTxt.getText();
			status = staTxt.getSelectedItem().toString();

			tableModel.addRow(new Object[] { String.valueOf(count),
						type,
						name,
						price,
						floor,
						status }
			);

			tableModel.fireTableDataChanged();

			JOptionPane.showMessageDialog(this, "Room details saved successfully..");
		} else {
			JOptionPane.showMessageDialog(this, "Please enter fields properly!");
		}
	}

}
