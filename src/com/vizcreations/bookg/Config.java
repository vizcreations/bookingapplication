/* Configuration of the application data */
package com.vizcreations.bookg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/* Configuration static class */

public class Config {

	private static String hotelName = "The Taj Mahal Palace"; // Not 'final' value as user may change
	private static String hotelRegId = "TJK1001";
	private static String hotelEstd = "Jan 1 1970";
	private static String hotelOwner = "Ratan Tata";
	private static String hotelAddr = "Gateway of India, Bombay";
	private static String hotelPhone = "022-57485869";

	private static double extraBedPrice = 1500.0;
	private static double serviceTax = 2.1;
	private static double VAT = 1.3;

	/* values not included in flat file */
	public static final String BOOKG_FILENAME = "_data/bookg.txt";
	public static final String ROOMT_FILENAME = "_data/roomt.txt";
	public static final String ROOM_FILENAME = "_data/room.txt";
	public static final String BOOKGROOM_FILENAME = "_data/bookgroom.txt"; // no getter/setter, directly accessible
	public static final String BOOKGCUST_FILENAME = "_data/bookgcust.txt"; // constant, not changeable runtime
	public static final String FILENAME = "_data/config.txt";

	public static void setHotelName(String sName) {
		hotelName = sName;
	}

	public static String getHotelName() {
		return hotelName;
	}

	public static void setHotelRegId(String sRegId) {
		hotelRegId = sRegId;
	}

	public static String getHotelRegId() {
		return hotelRegId;
	}

	public static void setHotelEstd(String sDate) {
		hotelEstd = sDate;
	}

	public static String getHotelEstd() {
		return hotelEstd;
	}

	public static void setHotelOwner(String sOwner) {
		hotelOwner = sOwner;
	}

	public static String getHotelOwner() {
		return hotelOwner;
	}

	public static void setHotelAddr(String sAddr) {
		hotelAddr = sAddr;
	}

	public static String getHotelAddr() {
		return hotelAddr;
	}

	public static void setHotelPhone(String sPhone) {
		hotelPhone = sPhone;
	}

	public static String getHotelPhone() {
		return hotelPhone;
	}

	public static void setExtraBedPrice(double sPrice) {
		extraBedPrice = sPrice;
	}

	public static double getExtraBedPrice() {
		return extraBedPrice;
	}

	public static void setServiceTax(double sTax) {
		serviceTax = sTax;
	}

	public static double getServiceTax() {
		return serviceTax;
	}

	public static void setVAT(double sVAT) {
		VAT = sVAT;
	}

	public static double getVAT() {
		return VAT;
	}
	
	public static void show() {
		// List out the data line by line
		System.out.format("%s\n\n%s\n\n%s\n\n%s\n\n%s\n\n%s\n\n%.2f\n\n%.2f\n\n%.2f%n",
			Config.getHotelName(),
			Config.getHotelRegId(),
			Config.getHotelEstd(),
			Config.getHotelOwner(),
			Config.getHotelAddr(),
			Config.getHotelPhone(),
			Config.getExtraBedPrice(),
			Config.getServiceTax(),
			Config.getVAT()
			);
	}
	
	public static void init() {
		// initialize data from file if exists
		BufferedReader in = null; // we're going to read lines
		String line;
		int counter = 0;

		try {
			in = new BufferedReader(new FileReader(Config.FILENAME));

			if(in != null) {

				while((line = in.readLine()) != null) {
					++counter;
					
					if(counter == 10) { // unexpected overflow
						break;
					}
					
					if(counter == 1) { // hotel name
						Config.setHotelName(line);
					} else if(counter == 2) {
						Config.setHotelRegId(line);
					} else if(counter == 3) {
						Config.setHotelEstd(line);
					} else if(counter == 4) {
						Config.setHotelOwner(line);
					} else if(counter == 5) {
						Config.setHotelAddr(line);
					} else if(counter == 6) {
						Config.setHotelPhone(line);
					} else if(counter == 7) {
						Config.setExtraBedPrice(Double.parseDouble(line));
					} else if(counter == 8) {
						Config.setServiceTax(Double.parseDouble(line));
					} else if(counter == 9) {
						Config.setVAT(Double.parseDouble(line));
					}

				}

			}

			if(in != null) {
				in.close();
			}
		} catch(NumberFormatException nfe) {

			System.err.println("Format error: " + nfe.getMessage());
			return;
		} catch(IOException e) {
			System.err.println("I/O error: " + e.getMessage());
		} finally {
		}
	}

	public static void save() throws NumberFormatException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(Config.FILENAME));
			if(out != null) {
					out.format("%s\n%s\n%s\n%s\n%s\n%s\n%.2f\n%.2f\n%.2f",
							Config.getHotelName(),
							Config.getHotelRegId(),
							Config.getHotelEstd(),
							Config.getHotelOwner(),
							Config.getHotelAddr(),
							Config.getHotelPhone(),
							Config.getExtraBedPrice(),
							Config.getServiceTax(),
							Config.getVAT()
						);

				out.close();
			}
		} catch(NumberFormatException nfe) {
			System.err.println("Format error: " + nfe.getMessage());
		} catch(IOException e) {
			System.err.println("I/O error: " + e.getMessage());
		}
	}
}
