/* full frame window for adding/editing a single roomtype */
package com.vizcreations.bookg;

import java.awt.*;
import java.awt.event.*; // for event listeners on buttons
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.Border;
//import javafx.scene.layout.Border;

import java.util.List;

public class RoomTypeGUIForm extends JFrame implements ActionListener { // it's a window itself

	private JLabel idLbl;
	private JTextField idTxt;
	private JLabel namLbl;
	private JTextField namTxt;
	private JLabel descLbl;
	private JTextArea descTxt;
	private JLabel prcLbl;
	private JTextField prcTxt;
	private JLabel staLbl;
	private JTextField staTxt;

	// temporary variable to hold data
//	private List<RoomType> roomTypes;
	private int numClicks = 0;
	private DefaultTableModel tableModel;

	private boolean editMode = false;

	public JTextField getIdTxt() { // returns the identifier for the textfield
		return idTxt;
	}

	public JTextField getNamTxt() {
		return namTxt;
	}

	public JTextArea getDescTxt() {
		return descTxt;
	}

	public JTextField getPrcTxt() {
		return prcTxt;
	}

	public JTextField getStaTxt() {
		return staTxt;
	}

	public RoomTypeGUIForm(boolean edit, long roomTId, DefaultTableModel sTableModel) { // constructor initializes elements and controls

		// fill up roomtypes from list object
//		roomTId = roomTId+1;
		editMode = edit;
//		roomTypes = RoomTypeList.getList();
		tableModel = sTableModel;
		// todo
		this.setSize(480, 320);
		this.getContentPane().setLayout(null);

		idLbl = new JLabel("ID");
//		idLbl.setBounds(10, 20, 150, 30);
		idLbl.setText("ID");
		idTxt = new JTextField();
//		idTxt.setBounds(170, 20, 50, 30);
		idTxt.setText(String.valueOf(roomTId));

		namLbl = new JLabel("Room type name");
		namLbl.setBounds(10, 20, 150, 30);
		namTxt = new JTextField();
		namTxt.setBounds(170, 20, 200, 30);

		descLbl = new JLabel("Description");
		descLbl.setBounds(10,60,150,30);
		descTxt = new JTextArea();
		Border border = BorderFactory.createLineBorder(Color.GRAY);
		descTxt.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		descTxt.setBounds(170, 60, 200, 75);

		prcLbl = new JLabel("Price");
		prcLbl.setBounds(10, 145, 150, 30);
		prcTxt = new JTextField();
		prcTxt.setBounds(170, 145, 200, 30);

		staLbl = new JLabel("Status");
//		staLbl.setBounds(10, 185, 150, 30);
		staTxt = new JTextField();
//		staTxt.setBounds(170, 185, 200, 30);
		staTxt.setText(String.valueOf("1"));
		if(editMode == true) {
//			for(RoomType roomT: roomTypes) {
//				if(roomTId == roomT.getId()) {
//					namTxt.setText(roomT.getName());
//					descTxt.setText(roomT.getDesc());
//					prcTxt.setText(String.valueOf(roomT.getPrice()));
//				}
//			}
		}

		this.add(idLbl);
		this.add(idTxt);
		this.add(namLbl);
		this.add(namTxt);
		this.add(descLbl);
		this.add(descTxt);
		this.add(prcLbl);
		this.add(prcTxt);
		this.add(staLbl);
		this.add(staTxt);
		JButton save = new JButton("Save");
		save.setBounds(170, 190, 120, 30);
//		save.addActionListener(new ActionListener() {
		save.addActionListener(this);
//			public void actionPerformed(ActionEvent e) {
				
//			}
//		});
		
		this.add(save);
//		this.pack();
//		this.setVisible(true);
	}

	// mandatory method definition to handle events
	public void actionPerformed(ActionEvent e) {
		// todo
//		RoomType roomT; // model
		String msg;
/*		numClicks++;
		if(numClicks > 1) {
			msg = "Sorry you cannot submit twice";
			JOptionPane.showMessageDialog(this, msg);
			System.exit(1);
		}*/
//		roomTypes = new LinkedList<RoomType>();
		
		// get list pointer from list object
//		roomTypes = RoomTypeList.getList(); // model
//			roomT = new RoomType(); // because we're saving

//		if(RoomTypeList.exists(Long.parseLong(idTxt.getText()),namTxt.getText()) == true) {
//			System.err.println("Roomtype "+namTxt.getText()+" already exists ...");
//			msg = "Roomtype " + namTxt.getText() + " already exists..";
//			JOptionPane.showMessageDialog(this, msg);
//			return;
//		}

		if(!idTxt.getText().isEmpty()
		&& !namTxt.getText().isEmpty()
		&& !prcTxt.getText().isEmpty()
		) {
			if(editMode == true) {
//				roomT = RoomTypeList.get(Long.parseLong(idTxt.getText()));
			}
//			roomT.setId(Long.parseLong(idTxt.getText()));
//			roomT.setName(namTxt.getText());
//			roomT.setDesc(descTxt.getText());
//			roomT.setPrice(Double.parseDouble(prcTxt.getText()));
//			roomT.setActive((Integer.parseInt(staTxt.getText()) == 1) ? true : false);
			if(editMode == false) {
//				roomTypes.add(roomT); // add to the original list pointer
			}
			// now save to flat file
//			RoomTypeList.save(); // saves it's data to file

			// save to table model
			if(editMode == false) { // new record
//				tableModel.addRow(new Object[] { String.valueOf(roomT.getId()),
//							roomT.getName(),
//							roomT.getDesc(),
//							String.valueOf(roomT.getPrice()),
//							(roomT.isActive()) ? "ACTIVE" : "INACTIVE" }
//				);
			}
			tableModel.fireTableDataChanged();

			msg = "Room type saved successfully..";
			this.dispose();
		} else {
			msg = "Please enter fields correctly!";
		}

		JOptionPane.showMessageDialog(this, msg);
	}

}
