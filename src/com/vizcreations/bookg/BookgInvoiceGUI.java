/* Summary and billing invoice of booking window */
package com.vizcreations.bookg;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;
import javax.swing.*;

public class BookgInvoiceGUI extends JFrame implements Printable, ActionListener {
	JFrame frameToPrint;

	public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
		
		if(page > 0) { /* we have only one page, and 'page' is zero based */
			return NO_SUCH_PAGE;
		}
		
		/* user (0, 0) is typically outside the imageable area, so we must
		 * translate by the X and Y values in the PageFormat to avoid clipping
		 */
		Graphics2D g2d = (Graphics2D)g;
		g2d.translate(pf.getImageableX(), pf.getImageableY());
		
		/* now print the window and it's visible contents */
		frameToPrint.printAll(g);
		
		/* tell the caller that this page is part of the printed document */
		return PAGE_EXISTS;
	}

	public BookgInvoiceGUI() { // the constructor useful to setup the window
//		JFrame invoiceFrame = new JFrame("Print Invoice");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int wide = screenSize.width;
		int high = screenSize.height;

		int halfWide = wide/2;
		int halfHigh = high/2;
		frameToPrint = this;
		setTitle("Invoice #435 - " + Config.getHotelName());
		setSize(screenSize.width, screenSize.height);
		setResizable(false);

		Box topBox = Box.createHorizontalBox();
		Box topLeftBox = Box.createVerticalBox();
		Box topRightBox = Box.createVerticalBox();
		Box mainBox = Box.createVerticalBox();
		Box signBox = Box.createHorizontalBox();
		Box signLeftBox = Box.createVerticalBox();
		Box signRightBox = Box.createVerticalBox();

//		setLocationByPlatform(true);

		JPanel invoiceContentPane = new JPanel();
		invoiceContentPane.setLayout(new BorderLayout());
		invoiceContentPane.setOpaque(true);
//		invoiceContentPane.setBackground(Color.WHITE);
		
		JPanel addrPane = new JPanel();
		JPanel custAddrPane = new JPanel();
//		GridLayout addrLayout = new GridLayout(1, 2);
		topBox.setPreferredSize(new Dimension(screenSize.width, 200));
		topBox.setBounds(0, 0, screenSize.width, 200);
		addrPane.setOpaque(true);
		addrPane.setPreferredSize(new Dimension(screenSize.width/2, 200));
		addrPane.setBounds(0, 0, 500, 200);
		addrPane.setLayout(null); /* fixed */

		custAddrPane.setOpaque(true);
		custAddrPane.setPreferredSize(new Dimension(screenSize.width/2, 200));
		custAddrPane.setBounds(0, 0, 500, 200);
		custAddrPane.setLayout(null);

		topLeftBox.setPreferredSize(new Dimension(screenSize.width/2, 200));
		topLeftBox.setBounds(0, 0, screenSize.width/2, 200);
		topLeftBox.add(addrPane);

		topRightBox.setPreferredSize(new Dimension(screenSize.width/2, 200));
		topRightBox.setBounds(0, 0, screenSize.width/2, 200);
		topRightBox.add(custAddrPane);
/*		
		JPanel billPane = new JPanel();
		billPane.setOpaque(true);
		billPane.setPreferredSize(new Dimension(800, 100));
		billPane.setLayout(null);
*/	
		JPanel signPane = new JPanel();
		JPanel custSignPane = new JPanel();
//		GridLayout signLayout = new GridLayout(1, 2);
		signPane.setOpaque(true);
		signPane.setLayout(null); /* fixed */
		custSignPane.setOpaque(true);
		custSignPane.setLayout(null);

		signLeftBox.setPreferredSize(new Dimension(screenSize.width/2, 200));
		signLeftBox.setBounds(0, 0, screenSize.width/2, 200);
		signRightBox.setPreferredSize(new Dimension(screenSize.width/2, 200));
		signRightBox.setBounds(0, 0, screenSize.width/2, 200);

		JLabel title = new JLabel("Invoice #435");
		title.setBounds(20, 20, 500, 25);
		title.setFont(new Font("Serif", Font.PLAIN, 24));
		JLabel hotNam = new JLabel(Config.getHotelName());
		hotNam.setBounds(20, 65, 500, 25);
		JLabel hotPhon = new JLabel(Config.getHotelPhone());
		hotPhon.setBounds(20, 115, 500, 25);
		JLabel date = new JLabel("Dated April 29, 2017");
		date.setBounds(20, 90, 500, 25);
		JLabel invoiceNo = new JLabel("Invoice no: AP# 42XX");
		invoiceNo.setBounds(20, 140, 500, 25);
		addrPane.add(title);
		addrPane.add(date);
		addrPane.add(hotNam);
		addrPane.add(hotPhon);
		addrPane.add(invoiceNo);
		
		JLabel custName = new JLabel("Ms. Pooja Sharma");
		custName.setBounds((halfWide/2), 65, 500, 25);
		JLabel addr = new JLabel("Chandni Chowk");
		addr.setBounds((halfWide/2), 90, 500, 25);
		JLabel phon = new JLabel("Phone: 886995966");
		phon.setBounds((halfWide/2), 115, 500, 25);
		JLabel arrDate = new JLabel("Arrival Date: April 15, 2017");
		arrDate.setBounds((halfWide/2), 140, 500, 25);
		custAddrPane.add(custName);
		custAddrPane.add(addr);
		custAddrPane.add(phon);
		custAddrPane.add(arrDate);

		topBox.add(topLeftBox);
		topBox.add(topRightBox);

		mainBox.add(topBox);

		JLabel cusSign = new JLabel("Customer Signature");
		cusSign.setBounds((halfWide/2), 50, 200, 25);
		JLabel cashSign = new JLabel("Cashier Signature");
		cashSign.setBounds(20, 50, 200, 25);
		custSignPane.add(cusSign);
		signPane.add(cashSign);

		JButton printButton = new JButton("Print");
		printButton.addActionListener(this);
		printButton.setBounds((halfWide/2), 85, 90, 25);
		custSignPane.add(printButton);

		signLeftBox.add(signPane);
		signRightBox.add(custSignPane);
//		title.setOpaque(true);
//		title.setBackground(Color.GREEN);
//		title.setBounds(50, 20, 50, 50);
		
/*
		invoiceContentPane.add(title);
		invoiceContentPane.add(date);
		invoiceContentPane.add(invoiceNo);
		invoiceContentPane.add(custName);
		invoiceContentPane.add(addr);
		invoiceContentPane.add(phon);
		invoiceContentPane.add(arrDate);
		invoiceContentPane.add(cusSign);
		invoiceContentPane.add(cashSign);
		invoiceContentPane.add(hotNam);
		invoiceContentPane.add(hotPhon);

		invoiceContentPane.add(printButton);
*/
//		invoiceContentPane.add(addrPane);

/*
		Insets insets = invoiceContentPane.getInsets();
		Dimension size =  title.getPreferredSize();
		title.setBounds(240 + insets.left, 5 + insets.top, size.width, size.height);
		date.setBounds(320 + insets.left, 40 + insets.top, size.width, size.height);
		invoiceNo.setBounds(320 + insets.left, 70 + insets.top, size.width+50, size.height);
		custName.setBounds(20 + insets.left, 40 + insets.top, size.width+80, size.height);
		addr.setBounds(20 + insets.left, 70 + insets.top, size.width+50, size.height);
		phon.setBounds(20 + insets.left, 100 + insets.top, size.width+50, size.height);
		arrDate.setBounds(20 + insets.left, 130 + insets.top, size.width+50, size.height);
		cusSign.setBounds(320 + insets.left,  280+ insets.top, size.width+100, size.height);
		cashSign.setBounds(20 + insets.left,  280+ insets.top, size.width+100, size.height);
		hotNam.setBounds(240 + insets.left, 320 + insets.top, size.width+80, size.height);
		hotPhon.setBounds(240 + insets.left, 340 + insets.top, size.width+80, size.height);
		printButton.setBounds(700, 500, 90, 30);
		printButton.addActionListener(this); // which object is listening action performed
*/

		String[] columnNames = { "ID", "Customer", "Checkin", "Check-out", "pax", "Paid amount", "Status" };
		Object[][] data = { 
				{ "1", "siva", "25-3-2017", "26-3-2017", "2", "1200", "Confirmed" },
				{ "", "", "", "", "", "Service tax", "1500.00" },
				{ "", "", "", "", "", "VAT", "600.00" },
				{ "", "", "", "", "", "Total", "7500.00" }
		};

		final JTable invoiceTable = new JTable(data, columnNames);
		invoiceTable.setPreferredScrollableViewportSize(new Dimension(screenSize.width, 100));
		invoiceTable.setFillsViewportHeight(true);
		JScrollPane invoiceScrollPane = new JScrollPane(invoiceTable);
//		invoiceContentPane.add(invoiceScrollPane);
		mainBox.add(invoiceScrollPane);

		signPane.setPreferredSize(new Dimension(500, 150));
		custSignPane.setPreferredSize(new Dimension(500, 150));

		signBox.add(signLeftBox);
		signBox.add(signRightBox);

		mainBox.add(signBox);
		invoiceContentPane.add(mainBox, BorderLayout.CENTER);
//		invoiceContentPane.add(signPane);
/*
		invoiceScrollPane.setBounds(20 + insets.left, 200 + insets.top, size.width+300, size.height+50);
*/
//		setContentPane(invoiceContentPane);

		setContentPane(invoiceContentPane);
//		invoiceFrame.pack();
		pack();
//		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) { // usually a print action

//		JOptionPane.showMessageDialog(this, "Coming soon..");
		PrinterJob job = PrinterJob.getPrinterJob();
		job.setPrintable(this);
		boolean ok = job.printDialog();
		
		if(ok) {
			try {
				job.print(); // calls the print() function/method
			} catch(PrinterException ex) {
				/* The job did not successfully complete */
			}
		}
	}

}
