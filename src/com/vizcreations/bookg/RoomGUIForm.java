/* full frame window to add/edit a single room */
package com.vizcreations.bookg;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.util.List;

public class RoomGUIForm extends JFrame implements ActionListener {

	// controls and other elements
	private JLabel idLbl;
	private JTextField idTxt;
	private JLabel rTIdLbl;
	private JComboBox rTIds;
	private JLabel namLbl;
	private JTextField namTxt;
	private JLabel floorLbl;
	private JTextField floorTxt;
	private JLabel staLbl;
	private JTextField staTxt;

//	private List<Room> rooms;
//	private List<RoomType> roomTypes;
	private int numClicks = 0;
	private DefaultTableModel tableModel;
	private boolean editMode = false; 

	public RoomGUIForm(boolean edit, long roomId, DefaultTableModel sTableModel) {
		// fill up rooms from list object
//			roomId = roomId+1;
			editMode = edit;
//			rooms = RoomList.getList();
//			roomTypes = RoomTypeList.getList();
			tableModel = sTableModel;
			this.setSize(480, 320);
			this.getContentPane().setLayout(null);
			this.setTitle("Add new room");

			idLbl = new JLabel("ID");
//			idLbl.setBounds(10, 20, 150, 30);
			idLbl.setText("ID");
			idTxt = new JTextField();
//			idTxt.setBounds(170, 20, 50, 30);
			idTxt.setText(String.valueOf(roomId));

			rTIdLbl = new JLabel("Room type Id");
			rTIdLbl.setBounds(10, 20, 150, 30);
			rTIds = new JComboBox();
			rTIds.setBounds(170, 20, 200, 30);
//			for(RoomType roomType: roomTypes) {
//				rTIds.addItem(roomType.getId());
//			}

			namLbl = new JLabel("Room name");
			namLbl.setBounds(10, 60, 150, 30);
			namTxt = new JTextField();
			namTxt.setBounds(170, 60, 200, 30);

			floorLbl = new JLabel("Floor");
			floorLbl.setBounds(10,100,150,30);
			floorTxt = new JTextField();
			floorTxt.setBounds(170, 100, 200, 30);

			staLbl = new JLabel("Status");
//			staLbl.setBounds(10, 190, 150, 30);
			staTxt = new JTextField();
//			staTxt.setBounds(170, 180, 200, 30);
			staTxt.setText(String.valueOf("1"));

			if(editMode == true) {
//				for(Room room: rooms) {
//					if(roomId == room.getId()) {
//						rTIds.setSelectedItem(room.getRoomTId());
//						namTxt.setText(room.getName());
//						floorTxt.setText(String.valueOf(room.getFloor()));
//					}
//				}
			}

			this.add(idLbl);
			this.add(idTxt);
			this.add(rTIdLbl);
			this.add(rTIds);
			this.add(namLbl);
			this.add(namTxt);
			this.add(floorLbl);
			this.add(floorTxt);
			this.add(staLbl);
			this.add(staTxt);
			JButton save = new JButton("Save");
			save.setBounds(170, 140, 120, 30);
			save.addActionListener(this);
			
			this.add(save);

//		this.pack();
	}

	public void actionPerformed(ActionEvent e) {
//		Room room; // model
		String msg;
//		numClicks++;
	//	if(numClicks > 1) {
//			msg = "Sorry you cannot submit twice";
//			JOptionPane.showMessageDialog(this, msg);
//			return;
	//		}
		// get list pointer from list object
//		room = new Room(); // because we're saving
//		if(RoomTypeList.idExists(Long.parseLong(rTIds.getSelectedItem().toString())) == false) {
//			System.err.println("Roomtype " +rTIds.getSelectedItem()+
//							" doesn't exists");
//			msg = "Room " + rTIds.getSelectedItem() + " doesn't exists..";
//			JOptionPane.showMessageDialog(this, msg);
//			return;
//		}

//		if(RoomList.exists(Long.parseLong(idTxt.getText()), namTxt.getText()) == true) {
//			System.err.println("Room " + namTxt.getText() + 
//							" already exists..");
//			msg = "Room " + namTxt.getText() + " already exists..";
//			JOptionPane.showMessageDialog(this, msg);
//			return;
//		}


		if(!idTxt.getText().isEmpty()
		&& !namTxt.getText().isEmpty()
		&& !floorTxt.getText().isEmpty()
		) {
			if(editMode == true) {
//				room = RoomList.get(Long.parseLong(idTxt.getText()));
			}
//			room.setId(Long.parseLong(idTxt.getText()));
//			room.setRoomTId(Long.parseLong(rTIds.getSelectedItem().toString()));
//			room.setName(namTxt.getText());
//			room.setFloor(Integer.parseInt(floorTxt.getText()));
//			room.setActive((Integer.parseInt(staTxt.getText()) == 1) ? true : false);
			if(editMode == false) {
//				rooms.add(room); // add to the original list pointer
			}
			// now save to flat file
//			RoomList.save(); // saves it's data to file
			if(editMode == false) {
//				tableModel.addRow(new Object[] { String.valueOf(room.getId()),
//						String.valueOf(room.getRoomTId()),
//						room.getName(),
//						String.valueOf(room.getFloor()),
//						(room.isActive()) ? "ACTIVE" : "INACTIVE" }
//				);
			}
/*			if(editMode == true) {
				int columnCount = tableModel.getColumnCount();
				int rowCount = tableModel.getRowCount();
				for(int i=0; i<rowCount; i++) {
					for(int j=0; j<columnCount; j++) {
//						if(tableModel.getValueAt(i, 0) == String.valueOf(room.getId())) {
//						Object cellValue = tableModel.getValueAt(i, j);
//						JOptionPane.showMessageDialog(this, cellValue);

//						}
					}
				}
			}*/
			tableModel.fireTableDataChanged();
			msg = "Room saved successfully..";
			this.dispose();
		} else {
			msg = "Please enter fields correctly!";
		}

		JOptionPane.showMessageDialog(this, msg);


	}
}
