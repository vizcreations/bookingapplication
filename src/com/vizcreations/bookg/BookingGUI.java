/* full frame window to add/edit a booking */
package com.vizcreations.bookg;

/* get the global */
import static com.vizcreations.bookg.BookgApp.bookgCount;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Iterator;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import javax.swing.table.DefaultTableModel;

public class BookingGUI extends JFrame implements ActionListener {

	private JLabel idLbl;
	private JTextField idTxt;
	private JLabel roomIdLbl;
//	private JTextField roomIdTxt;
	private JComboBox roomIdTxt;
	private JLabel exbedLbl;
//	private JTextField exbedTxt;
	private JSpinner exbedTxt;
//	private JComboBox<Integer> exbedTxt;

	/* customer data */
	private JLabel titleLbl;
	private JTextField titleTxt;
	private JLabel namLbl;
	private JTextField namTxt;
	private JLabel emlLbl;
	private JTextField emlTxt;
	private JLabel ageLbl;
	private JTextField ageTxt;
	private JLabel phonLbl;
	private JTextField phonTxt;
	private JLabel addrLbl;
	private JTextArea addrTxt;
	private JLabel pinLbl;
	private JTextField pinTxt;

	/* card data */
	private JLabel cardNameLbl;
	private JTextField cardNameTxt;
	private JLabel cardNoLbl;
	private JTextField cardNoTxt;
	private JLabel cardCodeLbl;
	private JTextField cardCodeTxt;
	private JLabel cardExpLbl;
	private JTextField cardExpTxt;

	private JLabel fromLbl;
//	private JTextField fromTxt;
	private JComboBox fromDateCmb;
	private JComboBox fromMonCmb;
	private JComboBox fromYrCmb;
	private JLabel toLbl;
//	private JTextField toTxt;
	private JComboBox toDateCmb;
	private JComboBox toMonCmb;
	private JComboBox toYrCmb;
	private JLabel paxLbl;
//	private JTextField paxTxt;
	private JSpinner paxTxt;

	private JLabel statLbl;
	private JComboBox statCmb;
//	private List<Room> rooms; // list of room objects

//	private List<Booking> bookgs;
//	private List<BookgRoom> bookgRooms;
	private Date myDate;
	private Date checkInDate;
	private Date checkOutDate;
	private long rawTime;
	private long checkInTime; // in seconds
	private long checkOutTime;
	private Pattern pattern;
	private Matcher matcher;
	private String[] mons;
	private Calendar cal;
	private Date curDate;
	private Date nextDate;
	private int curDay, curMon, curYear;
	private int nextDay, nextMon, nextYear;

	private String dateRegex = "(\\d+)(?:\\-)(\\d+)(?:\\-)(\\d+)";
	private double sTax;
	private double VAT;
	private Iterator iter;
	private int numClicks = 0;
	private DefaultTableModel tableModel;
	private long id;

	public BookingGUI(long bookgId, DefaultTableModel sTableModel) {

		JPanel mainPane = new JPanel();
		JPanel formPane = new JPanel();
//		JPanel boxPane = new JPanel();
		JPanel custPane = new JPanel();

		JPanel cardPane = new JPanel();

		final JFrame wnd = this;
		int i, j;
		cardPane.setOpaque(true);
		cardPane.setLayout(null);

		GridLayout gridLayout = new GridLayout(5, 2);
		Box leftBox = Box.createVerticalBox();
		Box rightBox = Box.createVerticalBox();
		Box topBox = Box.createHorizontalBox(); // contains first left and right
		Box mainBox = Box.createVerticalBox();
		Box bBox = Box.createHorizontalBox(); // contains bottom boxes
		Box bLeftBox = Box.createVerticalBox();
		Box bRightBox = Box.createHorizontalBox();

		JPanel helpPane = new JPanel();
		helpPane.setLayout(null);

		JPanel btnPane = new JPanel();
		btnPane.setLayout(null);

		mainPane.setOpaque(true);
//		boxPane.setLayout(new BoxLayout(boxPane, BoxLayout.Y_AXIS));
		formPane.setOpaque(true);
//		bookgs = BookgList.getList();
//		rooms = RoomList.getList();
		tableModel = sTableModel;
		id = bookgId;
//		this.getContentPane().setLayout(null); // own defined layout style
		setTitle("Add new booking - " + Config.getHotelName());
		this.setSize(800, 650);

		topBox.setPreferredSize(new Dimension(800, 600));
		leftBox.setPreferredSize(new Dimension(380, 350));
		rightBox.setPreferredSize(new Dimension(380, 600));

		bBox.setPreferredSize(new Dimension(800, 50));
		bLeftBox.setPreferredSize(new Dimension(380, 50));
		bRightBox.setPreferredSize(new Dimension(380, 50));

		formPane.setLocation(10, 10);
//		mainPane.setLayout(gridLayout);
		formPane.setLayout(null); /* fixed layout */
//		formPane.setMinimumSize(new Dimension(370, 250));
		formPane.setPreferredSize(new Dimension(380, 350));
//		formPane.setLayout(gridLayout);
//		formPane.setMaximumSize(new Dimension(370, 250));
//		formPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		custPane.setLayout(null); // because it's a form
		custPane.setOpaque(true);
		custPane.setPreferredSize(new Dimension(380, 350));
//		custPane.setMaximumSize(new Dimension(370, 440));

		cardPane.setPreferredSize(new Dimension(380, 200));

		idLbl = new JLabel("ID");
		idLbl.setText("ID");
		idTxt = new JTextField();
//		idTxt.setBounds(140, 20, 50, 30);
		idTxt.setText(String.valueOf(bookgId));

		roomIdLbl = new JLabel("Room Id");
		roomIdLbl.setBounds(20, 40, 120, 25);
//		roomIdTxt = new JTextField();
		roomIdTxt = new JComboBox();
		roomIdTxt.addItem("Room #101");
		roomIdTxt.addItem("Suite #45");

//		for(Room room: rooms) {
//			roomIdTxt.addItem(room.getName());
//		}


		roomIdTxt.setBounds(150, 40, 200, 25);

		exbedLbl = new JLabel("Extrabeds");
		exbedLbl.setBounds(20, 75, 120, 25);
//		exbedTxt = new JTextField();
		exbedTxt = new JSpinner();
		exbedTxt.setValue(1);
//		exbedTxt = new JComboBox<Integer>();
		exbedTxt.setBounds(150, 75, 50, 25);
/*		exbedTxt.addItem(new Integer(1));
		exbedTxt.addItem(new Integer(2));
		exbedTxt.addItem(new Integer(3));
		exbedTxt.addItem(new Integer(4));*/

//		String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

		mons = new String[] { "January",
					"February",
					"March",
					"April",
					"May",
					"June",
					"July",
					"August",
					"September",
					"October",
					"November",
					"December"
		};

		cal = Calendar.getInstance();
		curDate = new Date();
		curDay = curDate.getDate();
		curMon = curDate.getMonth();
		curYear = curDate.getYear()+1900;
		cal.setTime(curDate);
		cal.add(Calendar.DATE, 1); // add 1 day
		nextDate = cal.getTime();
		nextDay = nextDate.getDate();
		nextMon = nextDate.getMonth();
		nextYear = nextDate.getYear()+1900;

		fromLbl = new JLabel("Check-In date");
		fromLbl.setBounds(20, 110, 120, 25);
//		fromTxt = new JTextField();
//		fromTxt.setBounds(150, 230, 200, 25);
		fromDateCmb = new JComboBox();
		fromDateCmb.setBounds(150, 110, 55, 25);
		fromDateCmb.addItem("Day");
		// set up days in combo
		for(i=1; i<32; i++) {
			fromDateCmb.addItem(String.valueOf(i));
			if(curDay == i) {
				fromDateCmb.setSelectedIndex(i);	
			}
		}


		fromMonCmb = new JComboBox();
		fromMonCmb.setBounds(210, 110, 75, 25);
		fromMonCmb.addItem("Month");

		i = 0;
		for(String mon: mons) {
			fromMonCmb.addItem(mon);
			if(curMon == i) {
				fromMonCmb.setSelectedIndex(i+1);
			}
		}

		fromYrCmb = new JComboBox();
		fromYrCmb.setBounds(290, 110, 60, 25);
		fromYrCmb.addItem("Year");

		for(j=curYear,i=0; j<curYear+4; j++,i++) {
			fromYrCmb.addItem(String.valueOf(j));
			if(curYear == j) {
				fromYrCmb.setSelectedIndex(i+1); // because first index is a default string
			}
		}


		toLbl = new JLabel("Check-Out date");
		toLbl.setBounds(20, 145, 120, 25);
//		toTxt = new JTextField();
//		toTxt.setBounds(150, 265, 200, 25);
		toDateCmb = new JComboBox();
		toDateCmb.setBounds(150, 145, 55, 25);
		toDateCmb.addItem("Day");

		for(i=1; i<32; i++) {
			toDateCmb.addItem(String.valueOf(i));
			if(nextDay == i) {
				toDateCmb.setSelectedIndex(i);
			}
		}

		toMonCmb = new JComboBox();
		toMonCmb.setBounds(210, 145, 75, 25);
		toMonCmb.addItem("Month");

		i = 0;
		for(String mon: mons) {
			toMonCmb.addItem(mon);
			if(curMon == i) {
				toMonCmb.setSelectedIndex(i+1);
			}

			++i;
		}

		toYrCmb = new JComboBox();
		toYrCmb.setBounds(290, 145, 60, 25);
		toYrCmb.addItem("Year");

		for(j=nextYear,i=0; j<nextYear+4; j++,i++) {
			toYrCmb.addItem(String.valueOf(j));
			if(nextYear == j) {
				toYrCmb.setSelectedIndex(i+1);
			}
		}

		statLbl = new JLabel("Status");
		statLbl.setBounds(20, 180, 120, 25);
		statCmb = new JComboBox();
		statCmb.addItem("PENDING");
		statCmb.addItem("CONFIRMED");
		statCmb.setBounds(150, 180, 200, 25);

		JSeparator hsep = new JSeparator(JSeparator.HORIZONTAL);
//		hsep.setMinimumSize(new Dimension(350, 10));
		hsep.setPreferredSize(new Dimension(330, 10));
//		hsep.setMaximumSize(new Dimension(350, 10));
//		hsep.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		hsep.setBounds(20, 235, 330, 10);
		hsep.setLocation(20, 235);

		JLabel availTxt = new JLabel();
		availTxt.setText("<html>Please click Check Availability to show room types and\n" +
				"numbers available between the dates selected</html>");
		availTxt.setBounds(20, 245, 320, 50);
//		availTxt.setMinimumSize(new Dimension(350, 50));
		availTxt.setPreferredSize(new Dimension(350, 50));
		availTxt.setLocation(20, 245);
//		availTxt.setMaximumSize(new Dimension(350, 50));
		JButton avail = new JButton("Check availability");
//		avail.setLocation(20, 295);
		avail.setBounds(150, 305, 180, 25);
//		avail.setMinumumSize(new Dimension(180, 25));
		avail.setPreferredSize(new Dimension(180, 25));
//		avail.setMaximumSize(new Dimension(180, 25));
		avail.setLocation(150, 305);

		/* Customer frame controls */
		titleLbl = new JLabel("Title");
		titleLbl.setBounds(20, 40, 150, 25);
		titleTxt = new JTextField();
		titleTxt.setBounds(180, 40, 50, 25);

		namLbl = new JLabel("Customer name");
		namLbl.setBounds(20, 75, 150, 25);
		namTxt = new JTextField();
		namTxt.setBounds(180, 75, 200, 25);

		emlLbl = new JLabel("Customer email");
		emlLbl.setBounds(20, 110, 150, 25);
		emlTxt = new JTextField();
		emlTxt.setBounds(180, 110, 200, 25);

		ageLbl = new JLabel("Customer age");
		ageLbl.setBounds(20, 145, 150, 25);
		ageTxt = new JTextField();
		ageTxt.setBounds(180, 145, 50, 25);

		addrLbl = new JLabel("Customer address");
		addrLbl.setBounds(20, 180, 150, 25);
		addrTxt = new JTextArea();

		addrTxt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_TAB) {
					if(e.getModifiers() > 0) {
						addrTxt.transferFocusBackward();
					} else {
						addrTxt.transferFocus();
					}

					e.consume();
				}
			}

		});

		Border border = BorderFactory.createLineBorder(Color.GRAY);
		addrTxt.setBorder(BorderFactory.createCompoundBorder(border, 
									BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		addrTxt.setBounds(180, 180, 200, 50);
		addrTxt.setLineWrap(true);
		addrTxt.setWrapStyleWord(true);
		addrTxt.setFocusTraversalKeysEnabled(false);

		phonLbl = new JLabel("Customer phone");
		phonLbl.setBounds(20, 240, 150, 25);
		phonTxt = new JTextField();
		phonTxt.setBounds(180, 240, 200, 25);

		pinLbl = new JLabel("Pin code");
		pinLbl.setBounds(20, 275, 150, 25);
		pinTxt = new JTextField();
		pinTxt.setBounds(180, 275, 200, 25);

		// booking form
		paxLbl = new JLabel("Pax");
		paxLbl.setBounds(20, 310, 150, 25);
//		paxTxt = new JTextField();
		paxTxt = new JSpinner();
		paxTxt.setValue(1);
		paxTxt.setBounds(180, 310, 50, 25);

		/* payment form */
		cardNameLbl = new JLabel("Name on card");
		cardNameLbl.setBounds(20, 40, 150, 25);
		cardNameTxt = new JTextField();
		cardNameTxt.setBounds(180, 40, 200, 25);

		cardNoLbl = new JLabel("Card number");
		cardNoLbl.setBounds(20, 75, 150, 25);
		cardNoTxt = new JTextField();
		cardNoTxt.setBounds(180, 75, 200, 25);

		cardCodeLbl = new JLabel("Card code");
		cardCodeLbl.setBounds(20, 110, 150, 25);
		cardCodeTxt = new JTextField();
		cardCodeTxt.setBounds(180, 110, 100, 25);

		cardExpLbl = new JLabel("Card expiry date");
		cardExpLbl.setBounds(20, 145, 150, 25);
		cardExpTxt = new JTextField();
		cardExpTxt.setBounds(180, 145, 200, 25);

		cardPane.add(cardNameLbl);
		cardPane.add(cardNameTxt);
		cardPane.add(cardNoLbl);
		cardPane.add(cardNoTxt);
		cardPane.add(cardCodeLbl);
		cardPane.add(cardCodeTxt);
		cardPane.add(cardExpLbl);
		cardPane.add(cardExpTxt);

		formPane.add(idLbl);
		formPane.add(idTxt);
		formPane.add(roomIdLbl);
		formPane.add(roomIdTxt);
		formPane.add(exbedLbl);
		formPane.add(exbedTxt);

		formPane.add(fromLbl);
//		formPane.add(fromTxt);
		formPane.add(fromDateCmb);
		formPane.add(fromMonCmb);
		formPane.add(fromYrCmb);
		formPane.add(phonTxt);
		formPane.add(toLbl);
//		formPane.add(toTxt);
		formPane.add(toDateCmb);
		formPane.add(toMonCmb);
		formPane.add(toYrCmb);
		formPane.add(statLbl);
		formPane.add(statCmb);

		custPane.add(titleLbl);
		custPane.add(titleTxt);
		custPane.add(namLbl);
		custPane.add(namTxt);
		custPane.add(emlLbl);
		custPane.add(emlTxt);
		custPane.add(ageLbl);
		custPane.add(ageTxt);
		custPane.add(addrLbl);
		custPane.add(addrTxt);
		custPane.add(phonLbl);
		custPane.add(phonTxt);
		custPane.add(paxLbl);
		custPane.add(paxTxt);
		custPane.add(pinLbl);
		custPane.add(pinTxt);

		JButton priceAdj = new JButton("Price adj.");
		priceAdj.setBounds(20, 10, 120, 25);
		JButton save = new JButton("Save");
		save.setBounds(200, 10, 90, 25);
		save.addActionListener(this);
		JButton cancel = new JButton("Cancel");
		cancel.setBounds(300, 10, 90, 25);
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
//				JOptionPane.showMessageDialog(wnd, "Coming soon..");
				wnd.dispose(); /* close */
			}
		});

//		boxPane.add(formPane);
		formPane.add(hsep);
		formPane.add(availTxt);
		formPane.add(avail);
		leftBox.add(formPane);
//		leftBox.add(hsep);
//		leftBox.add(availTxt);
//		leftBox.add(avail);

//		custPane.add(save);
//		custPane.add(cancel);
		JButton helpBtn = new JButton("Help");
		helpBtn.setBounds(20, 10, 90, 25);
		helpBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(wnd, "Coming soon..");
			}
		});
		helpPane.add(helpBtn);
		bLeftBox.add(helpPane);

//		btnPane.add(priceAdj);
		btnPane.add(save);
		btnPane.add(cancel);
		bRightBox.add(btnPane);

		bBox.add(bLeftBox);
		bBox.add(bRightBox);

		mainPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		formPane.setBorder(BorderFactory.createTitledBorder("Booking details"));
		custPane.setBorder(BorderFactory.createTitledBorder("Customer details"));
		cardPane.setBorder(BorderFactory.createTitledBorder("Payment details"));

		rightBox.add(custPane);
		rightBox.add(cardPane);

		topBox.add(leftBox);
		topBox.add(rightBox);

		mainBox.add(topBox);
		mainBox.add(bBox);
		mainPane.setLayout(new BorderLayout());
		mainPane.add(mainBox, BorderLayout.CENTER);

		this.setContentPane(mainPane);
		this.pack();
		this.setResizable(false);
	}

	public void actionPerformed(ActionEvent e) {
//		Booking bookg; // model
//		Customer cust;
//		BookgRoom bookgRoom;
		String msg;
//		String[] roomIds;
//		String[] exBeds;
		myDate = new Date();
		checkInDate = new Date();
		checkOutDate = new Date();
		checkInTime = checkOutTime = 0;

		String custName;
		String custEmail;
		String custAddr;
		String custAge;
		String custPhone;

		String fromDate;
		String toDate;

		String pax;
		String totalCost;
		double roomPrice;
		double roomTotal = 0.0;
		double exbedPrice; // temp vars for assignment
		long noOfNights = 1;
		int extraBeds = 0;
		double extraBedPrice = 0.0;
		double total = 0.0;
		long roomCount;

		rawTime = myDate.getTime();
		rawTime /= 1000;
		numClicks++;
		if(numClicks > 1) {
			msg = "Sorry you cannot submit twice";
//			JOptionPane.showMessageDialog(this, msg);
//			System.exit(1);
//			return;
		}

		if(!idTxt.getText().isEmpty()
		&& !namTxt.getText().isEmpty()
//		&& !fromTxt.getText().isEmpty()
//		&& !toTxt.getText().isEmpty()
//		&& !roomIdTxt.getText().isEmpty()
		) {

			// get list pointer from list object
//			bookg = new Booking(); // because we're saving
//			cust = bookg.getCust();
//			bookgRooms = bookg.getBookgRooms();
//			bookg.setId(Long.parseLong(idTxt.getText()));
//			bookgRoom = new BookgRoom();
//			bookgRoom.setBookgId(bookg.getId());

//			roomIds = roomIdTxt.getText().split("[:]");
//			roomIds = String.valueOf(roomIdTxt.getSelectedIndex()+1).split("[:]");

//			exBeds = exbedTxt.getText().split("[:]");
//			exBeds = exbedTxt.getValue().toString().split("[:]");
			extraBeds = 0;
			roomCount = 0;

//			bookgRooms = new LinkedList<BookgRoom>();
//			bookgRooms = bookg.getBookgRooms();
//			for(String roomId: roomIds) {
//				if(RoomTypeList.idExists(Long.parseLong(roomId)) == false) {
//					System.err.println("Room " + roomId +
//									" doesn't exist");
//					msg = "Room " + roomId + " doesn't exist";
//					JOptionPane.showMessageDialog(this, msg);
//					System.exit(1);
//					return;
//				}

//				bookgRoom = new BookgRoom();
//				bookgRoom.setBookgId(bookg.getId());
//				bookgRoom.setRoomId(Long.parseLong(roomId));
//				roomPrice = RoomList.getPrice(bookgRoom.getRoomId());
//				bookgRoom.setRoomPrice(roomPrice);
//				roomTotal += bookgRoom.getRoomPrice();
//				roomCount++;

//				bookgRooms.add(bookgRoom);
//			}

//			iter = bookgRooms.iterator();
//			for(String exBedds: exBeds) { // exbedds for a single room

//				bookgRoom = (BookgRoom) iter.next(); // gets to first in first iteration
//				for(BookgRoom bkgRoom: bookgRooms) { // pull out each booking room in above list again loop
//				if(bookgRoom != null) {
//					bookgRoom.setExtraBeds(Integer.parseInt(exBedds));
//					if(bookgRoom.getExtraBeds() > 0) {
//						bookgRoom.setExtraBedPrice(Config.getExtraBedPrice());
//					}

//					extraBeds += bookgRoom.getExtraBeds();
//					bookgRoom = bookgRoom.getNext();
//				}

//			}

//			bookgRoom.setRoomId(Long.parseLong(roomIdTxt.getText()));
//			bookgRoom.setExtraBeds(Integer.parseInt(exbedTxt.getSelectedItem().toString()));
//			cust.setBookgId(bookg.getId());
//			cust.setName(namTxt.getText());
//			cust.setEmail(emlTxt.getText());
//			cust.setAge(Integer.parseInt(ageTxt.getText()));
//			cust.setAddr(addrTxt.getText());
//			cust.setPhone(phonTxt.getText());

			custName = namTxt.getText();
			custEmail = emlTxt.getText();
			custAge = ageTxt.getText();
			custAddr = addrTxt.getText();
			custPhone = phonTxt.getText();

			fromDate = fromDateCmb.getSelectedItem().toString() + "-" + 
					fromMonCmb.getSelectedItem().toString() + "-" + 
					fromYrCmb.getSelectedItem().toString();

			toDate = toDateCmb.getSelectedItem().toString() + "-" +
					toMonCmb.getSelectedItem().toString() + "-" +
					toYrCmb.getSelectedItem().toString();

			pax = paxTxt.getValue().toString();
			totalCost = "4500.00";
/*
			pattern = Pattern.compile(dateRegex);
			matcher = pattern.matcher(fromTxt.getText());
//msgBox(fromTxt.getText());
			if(matcher.find()) {

				checkInDate.setYear(Integer.parseInt(matcher.group(3)));
				checkInDate.setMonth(Integer.parseInt(matcher.group(2))-1);
				checkInDate.setDate(Integer.parseInt(matcher.group(1)));
				checkInDate.setHours(10);
				checkInDate.setMinutes(0);
				checkInDate.setSeconds(0);

				checkInTime = checkInDate.getTime();
				checkInTime /= 1000;
			}
//msgBox(String.valueOf(checkInTime));

			matcher = pattern.matcher(toTxt.getText());
//msgBox(toTxt.getText());

			if(matcher.find()) {
				checkOutDate.setYear(Integer.parseInt(matcher.group(3)));
				checkOutDate.setMonth(Integer.parseInt(matcher.group(2))-1);
				checkOutDate.setDate(Integer.parseInt(matcher.group(1)));
				checkOutDate.setHours(10);
				checkOutDate.setMinutes(0);
				checkOutDate.setSeconds(0);
				
				checkOutTime = checkOutDate.getTime();
				checkOutTime /= 1000;

			}

//msgBox(String.valueOf(checkOutTime));

			if(checkInTime < rawTime) {
				msg = "From date cannot be before today";
				JOptionPane.showMessageDialog(this, msg);
				return;
			}

			if(checkInTime > checkOutTime) {
				msg = "From date cannot be after to date";
				JOptionPane.showMessageDialog(this, msg);
				return;
			}
*/

//			bookg.setCheckIn(checkInTime);
//			bookg.setCheckOut(checkOutTime);
//			bookg.setPax(Integer.parseInt(paxTxt.getText()));
//			bookg.setPax(Integer.parseInt(paxTxt.getValue().toString()));
//			noOfNights = (checkOutTime - checkInTime)/86400 ;
//			roomTotal *= noOfNights;
//			extraBedPrice = Config.getExtraBedPrice() * noOfNights * extraBeds;
//			total += roomTotal + extraBedPrice;

//			sTax = (total * Config.getServiceTax())/100;
//			VAT = (total * Config.getVAT())/100;
//			bookg.setSTax(sTax);
//			bookg.setVAT(VAT);

//			total += sTax + VAT;
//			bookg.setTotalCost(total);
//			bookg.setPaidAmt(0.0);

//			bookgRooms.add(bookgRoom); 
//			bookgs.add(bookg);// add to the original list pointer

			// now save to flat file
//			BookgList.save();
//			BookgCustList.save();
//			BookgRoomList.save();

//			checkInTime = bookg.getCheckIn()*1000;
//			Date cI = new Date(checkInTime);
//			int cIY = cI.getYear();
//			int cIM = cI.getMonth()+1;
//			int cID = cI.getDate();
//			checkOutTime = bookg.getCheckOut()*1000;
//			Date cO = new Date(checkOutTime);
//			int cOY = cO.getYear();
//			int cOM = cO.getMonth()+1; // java month is in array index
//			int cOD = cO.getDate();

			/* new record */
			++id;
			++bookgCount; // update global variable
			tableModel.addRow(new Object[] { String.valueOf(bookgCount),
					custName,
					fromDate,
					toDate,
					pax,
					totalCost }
			);
		
			tableModel.fireTableDataChanged();
			msg = "Booking saved successfully..";

			this.dispose();
		} else {
			msg = "Please enter fields correctly!";
		}

		JOptionPane.showMessageDialog(this, msg);
	}

	private void msgBox(String msg) {
		JOptionPane.showMessageDialog(this, msg);
	}
}
