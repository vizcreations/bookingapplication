Booking Application Desktop Demo README
=======================================

A full blown to-be Booking Java application targeted for the
Linux desktop. However due to Java's nature
it can be built in any operating system and run as a
native application.


How to use?
===========
:~$ java -jar BookgApp.jar


How to use? [Windows]
=====================
1. Download Java
2. Double click the jar executable

How to use? [Mac]
=================
1. Dunno :(

This is a work-in-progress demo
options are wip, will arrive soon


vizcreations
